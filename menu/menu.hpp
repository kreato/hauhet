#pragma once

#include <string>
#include "../features/visuals.hpp"
#include "../singleton.hpp"
#include "imgui/imgui.h"
#include <d3dx9.h>

struct IDirect3DDevice9;

class Menu
    : public Singleton<Menu>
{
public:
    bool AimbotFirstTimeRender = true;
    bool TriggerFirstTimeRender = true;
    bool SkinsFirstTimeRender = true;
public:

    void Initialize();
    void Shutdown();

    void OnDeviceLost();
    void OnDeviceReset();

    void Render();

    void Toggle();

    bool IsVisible() const { return _visible; }

private:
    void CreateStyle();

    ImGuiStyle        _style;
    bool              _visible;
};