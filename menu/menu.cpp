//Project Hauhet
//Licensed under MIT
#include "Menu.hpp"
#define NOMINMAX
#include <Windows.h>
#include <chrono>
#include "../sdk/csgostructs.hpp"
#include "../helpers/input.hpp"
#include "../options.hpp"
#include "../DiscordRichPresence--master/include/discord_presence.hpp"
#include "../sdk/render/ui.hpp"
#include "../config.hpp"

#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui/imgui_internal.h"
#include "imgui/impl/imgui_impl_dx9.h"
#include "imgui/impl/imgui_impl_win32.h"
#include <mutex>
#include "..\menu\fonts\fonts.hpp"
#include "../features/prediction.hpp"
static char* sidebar_tabs[] = { "Aimbot", "Visuals","Skins", "Misc", "Config" };
extern LPDIRECT3DTEXTURE9 m_skin_texture = nullptr;
discord_presennce* discord_rich_presence;
template<size_t N>
void render_tabs(char* (&names)[N], int& activetab, float w, float h, bool sameline)
{
	bool values[N] = { false };

	values[activetab] = true;

	for (auto i = 0; i < N; ++i) {
		if (ImGui::ToggleButton(names[i], &values[i], ImVec2{ w, h })) {
			activetab = i;
		}
		if (sameline && i < N - 1)
			ImGui::SameLine();
	}
}

void Clantag()
{
	if (g_Options.hauhet && g_LocalPlayer)
	{
		Utils::SetClantag("hauhet");
	}
	else if (!g_Options.hauhet)
		Utils::SetClantag("");
}


constexpr static float get_sidebar_item_width() { return 65.0f; }
constexpr static float get_sidebar_item_height() { return  50.0f; }

ImVec2 get_sidebar_size()
{
	constexpr float padding = 10.0f;
	constexpr auto size_w = padding * 1.5f + get_sidebar_item_width();
	constexpr auto size_h = padding * 1.0f + (sizeof(sidebar_tabs) / sizeof(char*)) * get_sidebar_item_height();

	return ImVec2{ size_w, ImMax(325.0f, size_h) };
}


void RenderAimbot() {
	static bool editor_mode{ false };
	static std::string current_group{ "" };

	if (Menu::Get().AimbotFirstTimeRender) {
		if (g_LocalPlayer && g_EngineClient && g_EngineClient->IsInGame() && g_EngineClient->IsConnected()) {
			auto weapon = g_LocalPlayer->m_hActiveWeapon();
			if (weapon) {
				current_group = g_CustomWeaponGroups->GetGroupName(weapon->m_Item().m_iItemDefinitionIndex());
			}
		}
	}
	Menu::Get().AimbotFirstTimeRender = false;

	if (editor_mode) {
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(27 / 255.f, 27 / 255.f, 27 / 255.f, 1.f));
		if (ImGui::Button("Back", ImVec2(-1, 19)) || InputSys::Get().IsKeyDown(VK_ESCAPE))
			editor_mode = !editor_mode;
		g_CustomWeaponGroups->Menu(current_group);
		return;
	}


	ImGui::Columns(3, nullptr, false);

	ImGui::Text("Selector:");
	ImGui::BeginChild("##1", ImVec2(-1, -1), true); {
		if (ImGui::Button("Editor Mode", ImVec2(-1, 19)))
			editor_mode = !editor_mode;
		g_CustomWeaponGroups->RenderSelector(current_group, 39.f);
	};
	ImGui::EndChild();
	ImGui::NextColumn();

	legitbot_settings* settings = g_CustomWeaponGroups->GetSettings(current_group);

	ImGui::BeginChild("##aimbot.general", ImVec2(0, 0), true);
	{
		ImGui::Text("General");
		ImGui::Separator();
		ImGui::PushItemWidth(-1);
		ImGui::Checkbox("Enabled", &g_Options.aimbot_enable);
		ImGui::Checkbox("Deathmatch", &g_Options.aimbot_deathmatch);

		ImGui::Checkbox("Autopistol", &settings->autopistol);

		ImGui::Checkbox("Check Smoke", &settings->check_smoke);
		ImGui::Checkbox("Check Flash", &settings->check_flash);
		ImGui::Checkbox("Autowall", &settings->autowall);
		ImGui::Checkbox("Backtrack", &settings->enable_backtrack);
		ImGui::Checkbox("Silent", &settings->silent);
		ImGui::Checkbox("Humanize", &settings->humanize);

		ImGui::Checkbox("Only In Zoom", &settings->only_in_zoom);
		
		ImGui::PopItemWidth();
	}
	ImGui::EndChild();
	ImGui::NextColumn();
	ImGui::BeginChild("##aimbot.misc", ImVec2(0, 0), true);
	{
		ImGui::Text("Misc");
		ImGui::Separator();
		ImGui::PushItemWidth(-1);
		static char* priorities[] = {
		"Fov",
		"Distance"
		};
		static char* aim_types[] = {
		"Hitbox",
		"Nearest"
		};
		static char* fov_types[] = {
		"Static",
		"Dynamic"
		};
		static char* hitbox_list[] = {
		"Head",
		"Neck",
		"Lower Neck",
		"Body",
		"Thorax",
		"Chest",
		"Right Thing",
		"Left Thing", // 7
		};
		ImGui::Text("Aim Type:");
		ImGui::Combo("##aimbot.aim_type", &settings->aim_type, aim_types, IM_ARRAYSIZE(aim_types));
		if (settings->aim_type == 0) {
			ImGui::Text("Hitbox:");
			ImGui::Combo("##aimbot.hitbox", &settings->hitbox, hitbox_list, IM_ARRAYSIZE(hitbox_list));
		}
		ImGui::Text("Priority:");
		ImGui::Combo("##aimbot.priority", &settings->priority, priorities, IM_ARRAYSIZE(priorities));
		ImGui::Text("Fov Type:");
		ImGui::Combo("##aimbot.fov_type", &settings->fov_type, fov_types, IM_ARRAYSIZE(fov_types));
		ImGui::SliderFloat("##aimbot.fov", &settings->fov, 0, 20, "Fov: %.2f");
		if (settings->silent) {
			ImGui::SliderFloat("##aimbot.silent_fov", &settings->silent_fov, 0, 20, "Silent Fov: %.2f");
		}
		ImGui::SliderFloat("##aimbot.smooth", &settings->smooth, 1, 15, "Smooth: %.2f");
		if (!settings->silent) {
			ImGui::SliderInt("##aimbot.shot_delay", &settings->shot_delay, 0, 100, "Shot Delay: %.0f");
		}
		ImGui::SliderInt("##aimbot.kill_delay", &settings->kill_delay, 0, 1000, "Kill Delay: %.0f");
		if (settings->enable_backtrack) {
			ImGui::SliderInt("##aimbot_backtrack_ticks", &settings->backtrack_ticks, 0, 12, "BackTrack Ticks: %.0f");
		}
		if (settings->autowall) {
			ImGui::SliderInt("##aimbot.min_damage", &settings->min_damage, 1, 100, "Min Damage: %.0f");
		}
		ImGui::PopItemWidth();
		ImGui::Spacing();
		ImGui::Checkbox("RCS##aimbot.rcs", &settings->rcs);
		ImGui::Text("RCS Type:");
		static char* rcs_types[] = {
		"Standalone",
		"Aim"
		};
		ImGui::Combo("##aimbot.rcs_type", &settings->rcs_type, rcs_types, IM_ARRAYSIZE(rcs_types));
		ImGui::Checkbox("Custom Fov", &settings->rcs_fov_enabled);
		if (settings->rcs_fov_enabled) {
			ImGui::SliderFloat("##aimbot.rcs_fov", &settings->rcs_fov, 0, 20, "RCS Fov: %.2f");
		}
		ImGui::Checkbox("Custom Smooth", &settings->rcs_smooth_enabled);
		if (settings->rcs_smooth_enabled) {
			ImGui::SliderFloat("##aimbot.rcs_smooth", &settings->rcs_smooth, 1, 15, "RCS Smooth: %.2f");
		}
		ImGui::SliderInt("##aimbot.rcs_x", &settings->rcs_x, 0, 100, "RCS X: %.0f");
		ImGui::SliderInt("##aimbot.rcs_y", &settings->rcs_y, 0, 100, "RCS Y: %.0f");
		ImGui::SliderInt("##aimbot.rcs_start", &settings->rcs_start, 1, 30, "RCS Start: %.0f");
		ImGui::PopItemWidth();
	}
	ImGui::EndChild();
	ImGui::Columns(1, NULL, false);
}
const char* box_types[] = { "Default","Corners" };
const char* bind_types[] = { "Hold","Toggle","On","Off" };
const char* glow_modes[] = { "Default","Pulsing full","Obvodka","Obvodka pulsing" };
const char* soundesp_modes[] = { "Render","Beams" };
struct chamsExtraParams {
	bool* value;
	std::string name;
};
void removescope()
{
	if (g_Options.misc_remove_scope)
		g_CVar->FindVar("zoom_sensitivity_ratio_mouse")->SetValue(0);
	else
		g_CVar->FindVar("zoom_sensitivity_ratio_mouse")->SetValue(1);
}
void gay()
{
	if (g_Options.misc_rpc)
		discord_rich_presence->start();
	discord_rich_presence->update();
	if (g_Options.misc_rpc == 0)
		Discord_Shutdown();
}
void RenderEspTab()
{
	ImGui::Columns(3, nullptr, false);

	auto textSettings = [&](std::string show, std::string safe, float* size) -> void {
		ImGui::SameLine();

		bool openPopup = ImGui::Button(("+" + safe).c_str());

		if (openPopup)
			ImGui::OpenPopup((safe + "##popup").c_str());

		if (ImGui::BeginPopup((safe + "##popup").c_str())) {
			ImGui::PushItemWidth(160.f);
			if (ImGui::SliderFloat(("Size" + safe).c_str(), size, 8.f, 20.f))
				*size = std::clamp(*size, 8.f, 20.f);
			ImGui::PopItemWidth();
			ImGui::EndPopup();
		}
	};

	ImGui::Text("ESP:");
	ImGui::BeginChild("##1st", ImVec2(-1, 205), true); {
		ImGui::Checkbox("Boxes", &g_Options.enabled_box);
		ImGui::SameLine();
		bool openPopupBoxESP = ImGui::Button("+##box");
		if (openPopupBoxESP)
			ImGui::OpenPopup("##boxpopup");
		if (ImGui::BeginPopup("##boxpopup")) {
			ImGui::PushItemWidth(160.f);

			ImGui::Combo("Boxes type", &g_Options.box_type, box_types, 2);
			ImGui::Checkbox("Boxes outline", &g_Options.box_outline);
			ImGui::Checkbox("Boxes filled", &g_Options.box_filled);

			ImGui::PopItemWidth();
			ImGui::EndPopup();
		}

		ImGui::Checkbox("Skeleton", &g_Options.enabled_skeleton);
		ImGui::Checkbox("Name", &g_Options.name);
		textSettings("ESP Name Settings", "##namesettings", &g_Options.name_size);
		ImGui::Checkbox("HP", &g_Options.hp);
		ImGui::Checkbox("Armor", &g_Options.armor);
		ImGui::Checkbox("Ammos", &g_Options.ammo_bar);
		ImGui::Checkbox("Weapon", &g_Options.weapon);
		textSettings("ESP Weapon Settings", "##weaponsettings", &g_Options.weapon_size);
		ImGui::Checkbox("SnapLines", &g_Options.snipeline);
		ImGui::Checkbox("Head Dot", &g_Options.head_dot);
		ImGui::Checkbox("Flags", &g_Options.flags);
		ImGui::SameLine();
		bool openPopupFlags = ImGui::Button("+##flags");

		if (openPopupFlags)
			ImGui::OpenPopup("##flagspopup");

		if (ImGui::BeginPopup("##flagspopup")) {
			ImGui::PushItemWidth(160.f);

			ImGui::Checkbox("Reload", &g_Options.flags_reload);
			ImGui::SliderFloat("Size##reloadsize", &g_Options.flags_reload_size, 8.f, 20.f);

			ImGui::Checkbox("Scoped", &g_Options.flags_scoped);
			ImGui::SliderFloat("Size##scopedsize", &g_Options.flags_scoped_size, 8.f, 20.f);

			ImGui::Checkbox("Bomb", &g_Options.flags_bomb);
			ImGui::SliderFloat("Size##bombsize", &g_Options.flags_bomb_size, 8.f, 20.f);

			ImGui::Checkbox("Flashed", &g_Options.flags_flashed);
			ImGui::SliderFloat("Size##flashedsize", &g_Options.flags_flashed_size, 8.f, 20.f);

			ImGui::Checkbox("HK", &g_Options.flags_hk);
			if (ImGui::IsItemHovered()) {
				ImGui::SameLine();
				ImGui::Text(" | Note for D3F0LT: HK = HELMET/KEVLAR");
			}
			ImGui::SliderFloat("Size##hksize", &g_Options.flags_hk_size, 8.f, 20.f);


			ImGui::PopItemWidth();
			ImGui::EndPopup();
		}

		ImGui::Checkbox("Sound ESP", &g_Options.sound_esp);
		ImGui::SameLine();
		bool openPopupSoundESP = ImGui::Button("+##soundesp");

		if (openPopupSoundESP)
			ImGui::OpenPopup("##flagsoundesp");

		if (ImGui::BeginPopup("##flagsoundesp")) {
			ImGui::PushItemWidth(160.f);

			ImGui::Combo("Type", &g_Options.sound_esp_type, soundesp_modes, 2);
			ImGui::Checkbox("On team", &g_Options.sound_esp_onteam);
			ImGui::SliderFloat("SoundESP Time", &g_Options.sound_esp_time, 0.5f, 3.f);
			ImGui::SliderFloat("SoundESP Radius", &g_Options.sound_esp_radius, 15.f, 150.f);

			ImGui::PopItemWidth();
			ImGui::EndPopup();
		}
	};
	ImGui::EndChild();

	ImGui::Text("Glow:");
	ImGui::BeginChild("##glow", ImVec2(-1, -1), true); {
		ImGui::Checkbox("Enabled##glow", &g_Options.enabled_glow);
		ImGui::SameLine();
		bool openPopupGlowESP = ImGui::Button("+##glow");
		if (openPopupGlowESP)
			ImGui::OpenPopup("##glowpopup");
		if (ImGui::BeginPopup("##glowpopup")) {
			ImGui::PushItemWidth(160.f);

			ImGui::Combo("Glow type", &g_Options.type_glow, glow_modes, 4);
			ImGui::Checkbox("Glow Bloom", &g_Options.bloom_glow);

			ImGui::PopItemWidth();
			ImGui::EndPopup();
		}

		ImGui::Checkbox("Players##glow", &g_Options.players);
		ImGui::Checkbox("Team##glow", &g_Options.on_team);
		ImGui::Checkbox("Chickens##glow", &g_Options.on_chickens);
		ImGui::Checkbox("C4 Carrier##glow", &g_Options.c4_carrier);
		ImGui::Checkbox("C4 Planted##glow", &g_Options.c4_planted);
		ImGui::Checkbox("Defuse Kits##glow", &g_Options.defuse_kits);
		ImGui::Checkbox("Weapons##glow", &g_Options.weapons);
	};
	ImGui::EndChild();

	ImGui::NextColumn();

	ImGui::Text("Efekts:");
	ImGui::BeginChild("##second", ImVec2(-1, 195), true); {
		ImGui::Checkbox("Dormant", &g_Options.dormant);
		ImGui::Checkbox("On team", &g_Options.team);
		ImGui::Checkbox("Visible Only", &g_Options.visible_only);
		ImGui::Checkbox("Molotov Timer", &g_Options.molotov_timer);
		ImGui::Checkbox("No Visual Recoil", &g_Options.no_visual_recoil);
		ImGui::Checkbox("No Smoke", &g_Options.nosmoke);
		if (ImGui::SliderFloat("Nightmode Scale", &g_Options.nightmode_value, 0.05f, 1.f, "%.3f")) {
			g_Options.nightmode_value = std::clamp(g_Options.nightmode_value, 0.05f, 1.f);
			Globals::UpdateNightMode = true;
		}
		ImGui::Text("BLOOM:");

			ImGui::SliderFloat("Bloom Value", &g_Options.bloom_value, 0, 100.f);
			ImGui::SliderFloat("Ambient Value", &g_Options.model_ambient_value, 0, 10.f);
		


	//	ImGui::Checkbox("Third Person", &g_Visuals->options.third_person);
	//	ImGui::Keybind("Third Person Key", &g_Visuals->options.third_person_bind);
	//	if(g_Visuals->options.third_person)
	//		ImGui::SliderFloat("Distance", &g_Visuals->options.third_person_dist, 0.f, 150.f);

	};
	ImGui::EndChild();

	ImGui::Text("Chams: ");
	ImGui::BeginChild("##chams", ImVec2(-1, -1), true); {
		if (ImGui::ArrowButton("##left", ImGuiDir_Left) && g_Options.menuMaterial > 1)
			--g_Options.menuMaterial;
		ImGui::SameLine();
		ImGui::Text("%d", g_Options.menuMaterial);
		ImGui::SameLine();
		if (ImGui::ArrowButton("##right", ImGuiDir_Right) && g_Options.menuMaterial < int(g_Chams->chams_player_allies_all.materials.size()))
			++g_Options.menuMaterial;


			int matWorking = g_Options.menuMaterial - 1;
			const char* materials[] = {
				("Normal"),
				("Flat"),
				("Animated"),
				("Glass"),
				("Crystal"),
				("Silver"),
				("Glow armsrace"),
				("Glow"),
				("Gloss")
			};

			auto chamsSettings = [&](std::string show, std::string safe, ConfChams* settings, std::vector<chamsExtraParams> extraParams = {}) -> void {
				ImGui::Checkbox(show.c_str(), &settings->materials[matWorking].enabled);

				if (settings->materials[matWorking].enabled) {
					ImGui::SameLine();

					bool openPopup = ImGui::Button(("Settings" + safe).c_str());

					if (openPopup)
						ImGui::OpenPopup((safe + "##popup").c_str());

					if (ImGui::BeginPopup((safe + "##popup").c_str())) {
						ImGui::Text(show.c_str());
						ImGui::Separator();

						ImGui::PushItemWidth(120.f);
						ImGui::Combo(("Material" + safe).c_str(), &settings->materials[matWorking].material, materials, ARRAYSIZE(materials));
						ImGui::PopItemWidth();

						ImGui::Checkbox(("Do not remove model" + safe).c_str(), &settings->DoNotRemoveModel);
						ImGui::Checkbox(("Wireframe" + safe).c_str(), &settings->materials[matWorking].wireframe);
						ImGui::Checkbox(("Pulsing" + safe).c_str(), &settings->materials[matWorking].blinking);
						ImGui::Checkbox(("Health-Based" + safe).c_str(), &settings->materials[matWorking].healthBased);

						if (!extraParams.empty()) {
							ImGui::Spacing();
							for (auto& data : extraParams) {
								ImGui::Checkbox(data.name.c_str(), data.value);
							}
						}

						ImGui::Separator();

						ImGui::EndPopup();
					}
				}
			};

			chamsSettings("Enemies Visible", "##enemiesvisible", &g_Chams->chams_player_enemies_visible);
			chamsSettings("Enemies Occluded", "##enemiesoccluded", &g_Chams->chams_player_enemies_occluded);

			ImGui::Spacing();

			chamsSettings("Local Player", "##localplayer", &g_Chams->chams_player_local_player);
			chamsSettings("Weapons", "##weapons", &g_Chams->chams_weapons);
			chamsSettings("Hands", "##hands", &g_Chams->chams_hands);
			chamsSettings("Sleeves", "##sleeves", &g_Chams->chams_sleeves);
	};
	ImGui::EndChild();
	ImGui::NextColumn();

	ImGui::Text("Colors:");
	ImGui::BeginChild("##third", ImVec2(-1, -1), true); {
		ImGui::ColorEdit("Boxes Enemy Visible", g_Options.box_enemy_vis_clr);
		ImGui::ColorEdit("Boxes Enemy Invisible", g_Options.box_enemy_invis_clr);
		ImGui::ColorEdit("Boxes Fill", g_Options.box_fill_color);
		ImGui::ColorEdit("Boxes Outline", g_Options.box_outline_clr);
		ImGui::ColorEdit("HP Bar", g_Options.hp_clr);
		ImGui::ColorEdit("Armor Bar", g_Options.armor_clr);
		ImGui::ColorEdit("Ammo Bar", g_Options.ammobar_clr);
		ImGui::ColorEdit("Skeleton", g_Options.skeleton_clr);
		ImGui::ColorEdit("Name", g_Options.name_clr);
		ImGui::ColorEdit("Weapon", g_Options.weapon_clr);
		ImGui::ColorEdit("SnapLines", g_Options.snipeline_clr);
		ImGui::ColorEdit("Head Dot", g_Options.head_dot_clr);

		ImGui::ColorEdit("Molotov Timer", g_Options.molotov_timer_color);


		ImGui::ColorEdit("SoundESP circles", g_Options.sound_esp_clr);

		ImGui::ColorEdit("Glow Enemy Visible", g_Options.enemy_clr);
		ImGui::ColorEdit("Glow Enemy Invisible", g_Options.enemy_invis_clr);
		ImGui::ColorEdit("Glow Team Visible", g_Options.allies_clr);
		ImGui::ColorEdit("Glow Team Invisible", g_Options.allies_invis_clr);
		ImGui::ColorEdit("Glow Chickens", g_Options.chickens_clr);
		ImGui::ColorEdit("Glow C4 Carrier", g_Options.c4_carrier_clr);
		ImGui::ColorEdit("Glow C4 Planted", g_Options.c4_planted_clr);
		ImGui::ColorEdit("Glow Defuse Kits", g_Options.defuse_kits_clr);
		ImGui::ColorEdit("Glow Weapons", g_Options.weapons_clr);

		int matWorking = g_Options.menuMaterial - 1;

		ImGui::ColorEdit("Chams Enemies Visible", g_Chams->chams_player_enemies_visible.materials[matWorking].color);
		ImGui::ColorEdit("Chams Enemies Occluded", g_Chams->chams_player_enemies_occluded.materials[matWorking].color);
		ImGui::ColorEdit("Chams Backtrack", g_Chams->chams_player_backtrack.materials[matWorking].color);
		ImGui::ColorEdit("Chams Local Player", g_Chams->chams_player_local_player.materials[matWorking].color);
		ImGui::ColorEdit("Chams Weapons", g_Chams->chams_weapons.materials[matWorking].color);
		ImGui::ColorEdit("Chams Hands", g_Chams->chams_hands.materials[matWorking].color);
		ImGui::ColorEdit("Chams Sleeves", g_Chams->chams_sleeves.materials[matWorking].color);
	};
	ImGui::EndChild();
}

template <typename T>
void PickCurrentWeapon(int* idx, int* vec_idx, std::vector<T> arr) {
	if (!g_LocalPlayer) return;
	if (!g_EngineClient->IsInGame()) return;
	auto weapon = g_LocalPlayer->m_hActiveWeapon();
	if (!weapon) return;
	short wpn_idx = weapon->m_Item().m_iItemDefinitionIndex();
	if (g_Options.is_knife(wpn_idx)) {
		*idx = g_Change::WeaponNamesFull.at(0).definition_index;
		*vec_idx = 0;
		return;
	}
	auto wpn_it = std::find_if(arr.begin(), arr.end(), [wpn_idx](const T& a) {
		return a.definition_index == wpn_idx;
		});
	if (wpn_it != arr.end()) {
		*idx = wpn_idx;
		*vec_idx = std::abs(std::distance(arr.begin(), wpn_it));
	}
}
void RenderChangerTab() {
	ImGui::Columns(3, nullptr, false);

	static char search_bar[32];

	ImGui::Text("Selector:");
	ImGui::BeginChild("##1", ImVec2(-1, -1), true); {
		if (ImGui::Button("Current", ImVec2(-1, 19)) || Menu::Get().SkinsFirstTimeRender)
			PickCurrentWeapon(&g_Options.weapon_index_skins, &g_Options.weapon_vector_index_skins, g_Change::WeaponNamesFull);

		int iter = 0;
		for (auto& weapon : g_Change::WeaponNamesFull) {
			if (ImGui::ButtonT(weapon.name, ImVec2(-1, 19), g_Options.weapon_index_skins, weapon.definition_index, false, false)) {
				g_Options.weapon_index_skins = weapon.definition_index;
				g_Options.weapon_vector_index_skins = iter;
			}
			iter++;
		}
	};
	ImGui::EndChild();
	ImGui::NextColumn();


	auto& selected_entry = g_Options.Items[g_Change::WeaponNamesFull[g_Options.weapon_vector_index_skins].definition_index];
	selected_entry.definition_index = g_Options.weapon_index_skins;
	selected_entry.definition_vector_index = g_Options.weapon_vector_index_skins;
	bool is_gloves = false;


	ImGui::Text("Params:");
	ImGui::BeginChild("##2", ImVec2(-1, ((ImGui::GetCurrentWindow()->Size.y / 2) + 2)), true); {
		ImGui::InputInt("Seed", &selected_entry.seed);
		ImGui::Checkbox("Enable StatTrack", &selected_entry.enable_stat_track);
		ImGui::InputInt("StatTrak", &selected_entry.stat_trak);
		ImGui::InputText("Name##Skins", selected_entry.name, sizeof(selected_entry.name));
		if (ImGui::SliderFloat("Wear", &selected_entry.wear, FLT_MIN, 1.f, "%.10f", 5))
			selected_entry.wear = std::clamp(selected_entry.wear, FLT_MIN, 1.f);
		if (selected_entry.definition_index == WEAPON_KNIFE) {
			ImGui::Combo("Knife", &selected_entry.definition_override_vector_index, [](void* data, int idx, const char** out_text) {
				*out_text = g_Change::KnifeNames.at(idx).name;
				return true;
				}, nullptr, g_Change::KnifeNames.size(), 10);
			selected_entry.definition_override_index = g_Change::KnifeNames.at(selected_entry.definition_override_vector_index).definition_index;
		}
		else if (selected_entry.definition_index == GLOVE_T_SIDE) {
			ImGui::Combo("Glove", &selected_entry.definition_override_vector_index, [](void* data, int idx, const char** out_text) {
				*out_text = g_Change::GloveNames.at(idx).name;
				return true;
				}, nullptr, g_Change::GloveNames.size(), 10);
			selected_entry.definition_override_index = g_Change::GloveNames.at(selected_entry.definition_override_vector_index).definition_index;
			is_gloves = true;
		}
		else
			selected_entry.definition_override_vector_index = 0;

		static float next_enb_time = 0;
		float time_to_next_up = g_GlobalVars->curtime;

		time_to_next_up = std::clamp(next_enb_time - g_GlobalVars->curtime, 0.f, 1.f);

		std::string name = "Update (";
		name += std::to_string(time_to_next_up);
		name.erase(12, 16);
		name += ")";

		if (ImGui::Button(name.c_str(), ImVec2(-1, 19))) {
			if (next_enb_time <= g_GlobalVars->curtime) {
				Utils::ForceFullUpdate();
				next_enb_time = g_GlobalVars->curtime + 1.f;
			}
		}
	};
	ImGui::EndChild();

	ImGui::Text("Preview:");
	ImGui::BeginChild("##3", ImVec2(-1, -1), true, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse); {
		if (m_skin_texture) {
			ImGui::Image(m_skin_texture, ImGui::GetCurrentWindow()->Size);
		}
	};
	ImGui::EndChild();
	ImGui::NextColumn();

	ImGui::Text("Paintkit:");
	ImGui::BeginChild("##4", ImVec2(-1, -1), true); {
		static bool showAllWeapons = false;
		ImGui::Checkbox("All paint kits", &showAllWeapons);
		ImGui::InputText("Filter##search_bar_skins", search_bar, sizeof(search_bar));

		ImGui::ListBoxHeader("##paintkits", ImVec2(-1, -1));

		if (ImGui::Selectable("Default", selected_entry.paint_kit_index == 0)) {
			selected_entry.paint_kit_index = 0;
		}

		static auto GetColRar = [&](int rar) -> ImVec4 {
			switch (rar)
			{
			case 1:
				return ImColor(150, 150, 150, 255);
				break;
			case 2:
				return ImColor(100, 100, 255, 255);
				break;
			case 3:
				return ImColor(50, 50, 255, 255);
				break;
			case 4:
				return ImColor(255, 64, 242, 255);
				break;
			case 5:
				return ImColor(255, 50, 50, 255);
				break;
			case 6:
				return ImColor(255, 50, 50, 255);
				break;
			case 7:
				return ImColor(255, 196, 46, 255);
				break;
			default:
				return ImColor(150, 150, 150, 255);
				break;
			}
		};

		bool is_knife = selected_entry.definition_index == WEAPON_KNIFE || selected_entry.definition_index == WEAPON_KNIFE_T;
		int defindex = (is_knife || selected_entry.definition_index == GLOVE_T_SIDE || selected_entry.definition_index == GLOVE_CT_SIDE) ? selected_entry.definition_override_index : selected_entry.definition_index;
		std::string skinName = g_Change::GetWeaponNameById(defindex);
		if (skinName.compare("") != 0 || showAllWeapons)
		{
			std::string filter = std::string(search_bar);

			int curItem = -1;
			int s = 0;

			auto set = g_Options.weaponSkins[skinName];
			std::vector<std::string> list(set.size());
			std::vector<std::string> anti_repeat;

			std::copy(set.begin(), set.end(), list.begin());

			if (showAllWeapons) {
				list = {};
				for (auto [key, value] : g_Options.weaponSkins) {
					for (auto skin : value) {
						list.push_back(skin);
					}
				}
			}

			auto fnAntiRepeat = [&](std::string tx) -> bool {
				auto ret = std::find(anti_repeat.begin(), anti_repeat.end(), tx) == anti_repeat.end();
				if (ret)
					anti_repeat.push_back(tx);
				return ret;
			};

			for (auto skin : list) {
				int pk = g_Options.skinMap[skin].paintkit;
				if (pk == selected_entry.paint_kit_index)
					curItem = s;

				bool passed_filter = true;

				if (!filter.empty()) {
					std::string tempName = g_Options.skinNames[g_Options.skinMap[skin].tagName];
					std::string tempQuery = filter;
					std::transform(tempName.begin(), tempName.end(), tempName.begin(), ::tolower);
					std::transform(tempQuery.begin(), tempQuery.end(), tempQuery.begin(), ::tolower);
					if (tempName.find(tempQuery) == std::string::npos)
						passed_filter = false;
				}

				if (passed_filter && fnAntiRepeat(g_Options.skinNames[g_Options.skinMap[skin].tagName])) {
					ImGui::PushStyleColor(ImGuiCol_Text, is_knife ? GetColRar(6) : GetColRar(g_Options.skinMap[skin].rarity));
					if (ImGui::Selectable((g_Options.skinNames[g_Options.skinMap[skin].tagName] + "##" + skinName).c_str(), pk == selected_entry.paint_kit_index)) {
						selected_entry.paint_kit_index = pk;
					}
					ImGui::PopStyleColor();
				}

				s++;
			}

			static int saved_pk_index = -1;
			static int saved_w_index = -1;
			static int saved_wo_index = -1;
			if (saved_pk_index != selected_entry.paint_kit_index || saved_w_index != selected_entry.definition_index || saved_wo_index != selected_entry.definition_override_index || Menu::Get().SkinsFirstTimeRender) {
				saved_pk_index = selected_entry.paint_kit_index;
				saved_w_index = selected_entry.definition_index;
				saved_wo_index = selected_entry.definition_override_index;

				std::string validFname = "";
				if (saved_pk_index != 0)
					for (auto s : g_Options.weaponSkins[skinName]) {
						auto gg = g_Options.skinMap[s];
						if (gg.paintkit == selected_entry.paint_kit_index) {
							validFname = s;
							break;
						}
					}

				std::string full_path;

				if (validFname == "") {
					full_path = "resource/flash/econ/weapons/base_weapons/";
					if (!is_gloves)
						full_path += "weapon_";
					full_path += skinName + ".png";
				}
				else {
					full_path = ("resource/flash/econ/default_generated/");
					if (!is_gloves)
						full_path += "weapon_";
					full_path += skinName + "_" + validFname + "_light_large.png";
				}


				const auto handle = g_FileSys->open(full_path.c_str(), "r", "GAME");
				if (handle) {
					int file_len = g_FileSys->size(handle);
					char* image = new char[file_len];

					g_FileSys->read(image, file_len, handle);
					g_FileSys->close(handle);

					D3DXCreateTextureFromFileInMemory(g_D3DDevice9, image, file_len, &m_skin_texture);

					delete[] image;
				}
				else
					m_skin_texture = nullptr;
			}
		}
		ImGui::ListBoxFooter();
	};
	ImGui::EndChild();

	Menu::Get().SkinsFirstTimeRender = false;
}


void RenderMiscTab()
{
    bool placeholder_true = true;

    auto& style = ImGui::GetStyle();
    float group_w = ImGui::GetCurrentWindow()->Size.x - style.WindowPadding.x * 2;

    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
    ImGui::ToggleButton("MISC", &placeholder_true, ImVec2{ group_w, 25.0f });
    ImGui::PopStyleVar();

    ImGui::BeginGroupBox("##body_content");
    {
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ style.WindowPadding.x, style.ItemSpacing.y });
        ImGui::Columns(3, nullptr, false);
        ImGui::SetColumnOffset(1, group_w / 3.0f);
        ImGui::SetColumnOffset(2, 2 * group_w / 3.0f);
        ImGui::SetColumnOffset(3, group_w);
		
	//	ImGui::Checkbox("Fullbright", g_Options.misc_fullbright);
	//	ImGui::SliderFloat("Darkness", g_Options.misc_nightmode, 0.f, 1.f);
	//	ImGui::SliderFloat("Aspect Ratio", g_Options.misc_aspect_ratio, 0.f, 10.f);
		ImGui::Checkbox("Thirdperson", g_Options.misc_thirdperson); ImGui::SameLine();
		ImGui::Hotkey("##ThirdpersonHotkey", g_Options.misc_thirdperson_hotkey, ImVec2(60, 20));
	//	ImGui::Checkbox("Desync", g_Options.misc_desync);
        ImGui::Checkbox("Bunny hop", g_Options.misc_bhop);
	//	ImGui::Checkbox("Jumpbug", g_Options.misc_jumpbug); ImGui::SameLine();
	//	ImGui::Hotkey("##JumpBugHotKey", g_Options.misc_jumpbug_hotkey, ImVec2(60, 20));
	//	ImGui::Checkbox("Edge jump", g_Options.edgejump); ImGui::SameLine();
	//	ImGui::Hotkey("##Edgejumphotkey", g_Options.edgejump_hotkey, ImVec2(60, 20));
	//	ImGui::Checkbox("Duck in Air", g_Options.edgejump_duck_in_air);
	//	ImGui::Checkbox("Remove scope", g_Options.misc_remove_scope);
		ImGui::Checkbox("Watermark##hc", g_Options.misc_watermark);
		ImGui::Checkbox("Clantag", g_Options.hauhet);
		ImGui::Checkbox("Discord RPC", g_Options.misc_rpc);
	//	ImGui::Checkbox("Silent Walk", g_Options.misc_silentwalk);

		ImGui::NextColumn();
		
	//	ImGui::Checkbox("KeyBind", &g_Options.show_keybinds);
        ImGui::SliderInt("viewmodel fov", g_Options.viewmodel_fov, 68, 120);
		//ImGui::SliderInt("Offset X", g_Options.viewmodel_offset_x, -20, 20);
		//ImGui::SliderInt("Offset Y", g_Options.viewmodel_offset_y, -20, 20);
		//ImGui::SliderInt("Offset Z", g_Options.viewmodel_offset_z, -20, 20);
        
		ImGui::Columns(1, nullptr, false);
        ImGui::PopStyleVar();
    }
    ImGui::EndGroupBox();
}

void Menu::Initialize()
{
	CreateStyle();

    _visible = true;
}

void Menu::Shutdown()
{
    ImGui_ImplDX9_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

void Menu::OnDeviceLost()
{
    ImGui_ImplDX9_InvalidateDeviceObjects();
}

void Menu::OnDeviceReset()
{
    ImGui_ImplDX9_CreateDeviceObjects();
}

void SpectatorList() {
	int specs = 0;
	std::string spect = "";

	if (g_EngineClient->IsInGame() && g_EngineClient->IsConnected()) {
		int localIndex = g_EngineClient->GetLocalPlayer();
		C_BasePlayer* pLocalEntity = C_BasePlayer::GetPlayerByIndex(localIndex);
		if (pLocalEntity) {
			for (int i = 0; i < g_EngineClient->GetMaxClients(); i++) {
				C_BasePlayer* pBaseEntity = C_BasePlayer::GetPlayerByIndex(i);
				if (!pBaseEntity)										     continue;
				if (pBaseEntity->m_iHealth() > 0)							 continue;
				if (pBaseEntity == pLocalEntity)							 continue;
				if (pBaseEntity->IsDormant())								 continue;
				if (pBaseEntity->m_hObserverTarget() != pLocalEntity)		 continue;
				player_info_t pInfo;
				g_EngineClient->GetPlayerInfo(pBaseEntity->EntIndex(), &pInfo);
				if (pInfo.ishltv) continue;

				spect += pInfo.szName;
				spect += "\n";
				specs++;
			}
		}
	}
	ImGui::PushStyleVar(ImGuiStyleVar_WindowTitleAlign, ImVec2(0.5f, 0.5f));
	if (ImGui::Begin("Spectator List", nullptr, ImVec2(0, 0), 0.4F, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar)) {
		if (specs > 0) spect += "\n";

		ImVec2 size = ImGui::CalcTextSize(spect.c_str());
		ImGui::SetWindowSize(ImVec2(200, 25 + size.y));
		ImGui::Text(spect.c_str());
	}
	ImGui::End();
	ImGui::PopStyleVar();
}

void Menu::Render()
{
	ImGui::GetIO().MouseDrawCursor = _visible;

	if (g_Options.misc_spectators)
		SpectatorList();

	if (!_visible)
		return;

	const auto sidebar_size = get_sidebar_size();
	static int active_sidebar_tab = 0;

	g_KeyBinds->Draw();

	ImGui::SetNextWindowPos(ImVec2{ 0, 0 }, ImGuiSetCond_Once);
	ImGui::SetNextWindowSize(ImVec2{ 650, 350 }, ImGuiSetCond_Once);

	if (ImGui::Begin("CSGOSimple",
		&_visible,
		ImGuiWindowFlags_NoCollapse |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoTitleBar)) {
		//initializing shit
		gay(); // RPC
		removescope(); //remove scope
	//	edge_jump(); // edgejump/bug
	//	jumpbug(); // jumpbug
		Clantag(); //clantag
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
		{
			ImGui::BeginGroupBox("##sidebar", sidebar_size);
			{
				render_tabs(sidebar_tabs, active_sidebar_tab, get_sidebar_item_width(), get_sidebar_item_height(), false);
			}
		//	ImGui::PopFont();
			ImGui::EndGroupBox();
		}
		ImGui::PopStyleVar();
		ImGui::SameLine();

		auto size = ImVec2{ 0.0f, sidebar_size.y };
		
		ImGui::BeginGroupBox("##body", size);
		if (active_sidebar_tab == 0) {
			RenderAimbot();
		}
		else if (active_sidebar_tab == 1) {
			RenderEspTab();
		}
		else if (active_sidebar_tab == 2) {
			RenderChangerTab();
		}
		else if (active_sidebar_tab == 3) {
			RenderMiscTab();
		}
		else if (active_sidebar_tab == 4) {
			g_Config->Menu();
		}
		ImGui::EndGroupBox();

		ImGui::End();
	}
}



void Menu::Toggle()
{
    _visible = !_visible;
}

void Menu::CreateStyle()
{
#define HI(v)   ImVec4(0.502f, 0.075f, 0.256f, v)
#define MED(v)  ImVec4(0.455f, 0.198f, 0.301f, v)
#define LOW(v)  ImVec4(0.232f, 0.201f, 0.271f, v)
#define BG(v)   ImVec4(0.200f, 0.220f, 0.270f, v)
	// text
#define TEXT(v) ImVec4(0.860f, 0.930f, 0.890f, v)
	ImGui::StyleColorsDark();
	ImGui::SetColorEditOptions(ImGuiColorEditFlags_HEX);
	_style.Colors[ImGuiCol_Text] = TEXT(0.78f);
	_style.Colors[ImGuiCol_TextDisabled] = TEXT(0.28f);
	_style.Colors[ImGuiCol_WindowBg] = ImVec4(0.13f, 0.14f, 0.17f, 1.00f);
	_style.Colors[ImGuiCol_ChildWindowBg] = BG(0.58f);
	_style.Colors[ImGuiCol_PopupBg] = BG(0.9f);
	_style.Colors[ImGuiCol_Border] = ImVec4(0.31f, 0.31f, 1.00f, 0.00f);
	_style.Colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	_style.Colors[ImGuiCol_FrameBg] = BG(1.00f);
	_style.Colors[ImGuiCol_FrameBgHovered] = MED(0.78f);
	_style.Colors[ImGuiCol_FrameBgActive] = MED(1.00f);
	_style.Colors[ImGuiCol_TitleBg] = LOW(1.00f);
	_style.Colors[ImGuiCol_TitleBgActive] = HI(1.00f);
	_style.Colors[ImGuiCol_TitleBgCollapsed] = BG(0.75f);
	_style.Colors[ImGuiCol_MenuBarBg] = BG(0.47f);
	_style.Colors[ImGuiCol_ScrollbarBg] = BG(1.00f);
	_style.Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.09f, 0.15f, 0.16f, 1.00f);
	_style.Colors[ImGuiCol_ScrollbarGrabHovered] = MED(0.78f);
	_style.Colors[ImGuiCol_ScrollbarGrabActive] = MED(1.00f);
	_style.Colors[ImGuiCol_CheckMark] = ImVec4(0.71f, 0.22f, 0.27f, 1.00f);
	_style.Colors[ImGuiCol_SliderGrab] = ImVec4(0.47f, 0.77f, 0.83f, 0.14f);
	_style.Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.71f, 0.22f, 0.27f, 1.00f);
	_style.Colors[ImGuiCol_Button] = ImVec4(0.47f, 0.77f, 0.83f, 0.14f);
	_style.Colors[ImGuiCol_ButtonHovered] = MED(0.86f);
	_style.Colors[ImGuiCol_ButtonActive] = MED(1.00f);
	_style.Colors[ImGuiCol_Header] = MED(0.76f);
	_style.Colors[ImGuiCol_HeaderHovered] = MED(0.86f);
	_style.Colors[ImGuiCol_HeaderActive] = HI(1.00f);
	_style.Colors[ImGuiCol_Column] = ImVec4(0.14f, 0.16f, 0.19f, 1.00f);
	_style.Colors[ImGuiCol_ColumnHovered] = MED(0.78f);
	_style.Colors[ImGuiCol_ColumnActive] = MED(1.00f);
	_style.Colors[ImGuiCol_ResizeGrip] = ImVec4(0.47f, 0.77f, 0.83f, 0.04f);
	_style.Colors[ImGuiCol_ResizeGripHovered] = MED(0.78f);
	_style.Colors[ImGuiCol_ResizeGripActive] = MED(1.00f);
	_style.Colors[ImGuiCol_PlotLines] = TEXT(0.63f);
	_style.Colors[ImGuiCol_PlotLinesHovered] = MED(1.00f);
	_style.Colors[ImGuiCol_PlotHistogram] = TEXT(0.63f);
	_style.Colors[ImGuiCol_PlotHistogramHovered] = MED(1.00f);
	_style.Colors[ImGuiCol_TextSelectedBg] = MED(0.43f);
	// [...]
	_style.Colors[ImGuiCol_ModalWindowDarkening] = BG(0.73f);
	_style.WindowPadding = ImVec2(9.000000f, 6.000000f);
	_style.FramePadding = ImVec2(3.000000f, 3.000000f);
	_style.ItemSpacing = ImVec2(8.000000f, 4.000000f);
	_style.ItemInnerSpacing = ImVec2(4.000000f, 4.000000f);
	_style.IndentSpacing = 21.000000f;
	_style.ScrollbarSize = 1.000000f;
	_style.GrabMinSize = 1.000000f;
	_style.WindowBorderSize = 1.000000f;
	_style.ChildBorderSize = 1.000000f;
	_style.PopupBorderSize = 1.000000f;
	_style.FrameBorderSize = 1.000000f;
	_style.WindowRounding = 12.000000f;
	_style.ChildRounding = 0.000000f;
	_style.FrameRounding = 0.000000f;
	_style.PopupRounding = 0.000000f;
	_style.ScrollbarRounding = 0.000000f;
	_style.GrabRounding = 0.000000f;
	_style.WindowTitleAlign = ImVec2(0.000000f, 0.500000f);
	_style.ButtonTextAlign = ImVec2(0.500000f, 0.500000f);
	_style.SelectableTextAlign = ImVec2(0.500000f, 0.500000f);


	ImGui::GetStyle() = _style;
}

