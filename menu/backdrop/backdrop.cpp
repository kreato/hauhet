#include "backdrop.h"
#include "../menu.hpp"
#include "../../sdk/csgostructs.hpp"
#include "../../singleton.hpp"

std::vector<Dot*> Dots;

void Backdrop::Render() {
	if (!Menu::Get().IsVisible())
		return;

	static int Width, Height;
	g_EngineClient->GetScreenSize(Width, Height);

	int s = rand() % 130;

	if (s == 0)
		Dots.push_back(new Dot(Vector2D(rand() % (int)Width, -16), Vector2D((rand() % 7) - 3, rand() % 3 + 1)));
	else if (s == 1)
		Dots.push_back(new Dot(Vector2D(rand() % (int)Width, (int)Height + 16), Vector2D((rand() % 7) - 3, -1 * (rand() % 3 + 1))));
	else if (s == 2)
		Dots.push_back(new Dot(Vector2D(-16, rand() % (int)Height), Vector2D(rand() % 3 + 1, (rand() % 7) - 3)));
	else if (s == 3)
		Dots.push_back(new Dot(Vector2D((int)Width + 16, rand() % (int)Height), Vector2D(-1 * (rand() % 3 + 1), (rand() % 7) - 3)));

	for (auto i = Dots.begin(); i < Dots.end();)
	{
		if ((*i)->RelativePosition.y < -20 || (*i)->RelativePosition.y > Height + 20 || (*i)->RelativePosition.x < -20 || (*i)->RelativePosition.x > Width + 20) {
			delete (*i);
			i = Dots.erase(i);
		}
		else {
			(*i)->RelativePosition = (*i)->RelativePosition + (*i)->Velocity * (0.3f);
			i++;
		}
	}
	for (auto i = Dots.begin(); i < Dots.end(); i++)
		(*i)->Draw();
}