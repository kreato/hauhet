#pragma once
#include "../../menu/imgui/imgui.h"
#define IMGUI_DEFINE_MATH_OPERATORS
#include "../../menu/imgui/imgui_internal.h"
#include <Windows.h>
#include <string>
#include <functional>
#include <vector>
#include "../misc/Color.hpp"
#include "../../helpers/clip.h"
#include "../../features/keybinds.hpp"

namespace ImGui {
	bool ToggleButton(const char * label, bool * v, const ImVec2 & size_arg = ImVec2(0, 0));
	bool Combo(const char * label, int * currIndex, std::vector<std::string>& values);
	bool BeginGroupBox(const char * name, const ImVec2 & size_arg = ImVec2(0, 0));
	bool Hotkey(const char* label, int* k, const ImVec2& size_arg);
	void EndGroupBox();
	float CalcMaxPopupHeightFromItemCount(int items_count);
	bool Keybind(const char* str_id, CKeyBind* kbind);
	bool ListBox(const char * label, int * current_item, std::string items[], int items_count, int height_items);
	bool ListBox(const char * label, int * current_item, std::function<const char*(int)> lambda, int items_count, int height_in_items);
	bool Combo(const char * label, int * current_item, std::function<const char*(int)> lambda, int items_count, int height_in_items);
	bool ButtonExT(const char* label, const ImVec2& size_arg, ImGuiButtonFlags flags, int page, int in, bool border, ImColor clr);
	bool ButtonT(const char* label, const ImVec2& size_arg, int page, int in, ImColor clr, bool border);
	bool ColorEdit(const char* label, Color& v);
}