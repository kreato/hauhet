#include "config.hpp"
#include "options.hpp"
#include "menu/menu.hpp"


inline bool file_exists(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}
void Config::PushItem(bool* pointer, std::string category, std::string name, bool def_value) {
	this->booleans.push_back({ pointer, category, name, def_value });
}
void Config::PushItem(int* pointer, std::string category, std::string name, int def_value) {
	this->ints.push_back({ pointer, category, name, def_value });
}
void Config::PushItem(float* pointer, std::string category, std::string name, float def_value) {
	this->floats.push_back({ pointer, category, name, def_value });
}
void Config::PushItem(Color* pointer, std::string category, std::string name, Color def_value) {
	this->colors.push_back({ pointer, category, name, def_value });
}
void Config::PushItem(CKeyBind* pointer, std::string category, std::string name, CKeyBind def_value) {
	this->keybinds.push_back({ pointer, category, name, def_value });
	g_KeyBinds->DeclareKeybind(pointer);
}

void Config::SetupValues() {
	// AIM
	g_Aimbot.SetupValues();
	// BOX
	g_Config->PushItem(&g_Options.enabled_box, "esp", "enabled_box", g_Options.enabled_box);
	g_Config->PushItem(&g_Options.box_outline, "esp", "box_outline", g_Options.box_outline);
	g_Config->PushItem(&g_Options.box_filled, "esp", "box_filled", g_Options.box_filled);
	g_Config->PushItem(&g_Options.box_type, "esp", "box_type", g_Options.box_type);
	g_Config->PushItem(&g_Options.box_outline_clr, "esp", "box_outline_clr", g_Options.box_outline_clr);
	g_Config->PushItem(&g_Options.box_allies_vis_clr, "esp", "box_allies_vis_clr", g_Options.box_allies_vis_clr);
	g_Config->PushItem(&g_Options.box_enemy_vis_clr, "esp", "box_enemy_vis_clr", g_Options.box_enemy_vis_clr);
	g_Config->PushItem(&g_Options.box_allies_invis_clr, "esp", "box_allies_invis_clr", g_Options.box_allies_invis_clr);
	g_Config->PushItem(&g_Options.box_enemy_invis_clr, "esp", "box_enemy_invis_clr", g_Options.box_enemy_invis_clr);
	g_Config->PushItem(&g_Options.box_fill_color, "esp", "box_fill_color", g_Options.box_fill_color);
	// SKELETON
	g_Config->PushItem(&g_Options.enabled_skeleton, "esp", "enabled_skeleton", g_Options.enabled_skeleton);
	g_Config->PushItem(&g_Options.skeleton_clr, "esp", "skeleton_clr", g_Options.skeleton_clr);
	// ESP_OTHER
	g_Config->PushItem(&g_Options.hp, "esp", "hp", g_Options.hp);
	g_Config->PushItem(&g_Options.name, "esp", "name", g_Options.name);
	g_Config->PushItem(&g_Options.weapon, "esp", "weapon", g_Options.weapon);
	g_Config->PushItem(&g_Options.ammo_bar, "esp", "ammo_bar", g_Options.ammo_bar);
	g_Config->PushItem(&g_Options.molotov_timer, "esp", "molotov_timer", g_Options.molotov_timer);
	g_Config->PushItem(&g_Options.molotov_timer_color, "esp", "molotov_timer_color", g_Options.molotov_timer_color);
	g_Config->PushItem(&g_Options.nightmode_value, "esp", "nightmode_value", g_Options.nightmode_value);
	g_Config->PushItem(&g_Options.nosmoke, "esp", "nosmoke", g_Options.nosmoke);
	g_Config->PushItem(&g_Options.no_visual_recoil, "esp", "no_visual_recoil", g_Options.no_visual_recoil);

	g_Config->PushItem(&g_Options.armor, "esp", "armor", g_Options.armor);
	g_Config->PushItem(&g_Options.snipeline, "esp", "snipeline", g_Options.snipeline);
	g_Config->PushItem(&g_Options.head_dot, "esp", "head_dot", g_Options.head_dot);
	g_Config->PushItem(&g_Options.team, "esp", "team", g_Options.team);
	g_Config->PushItem(&g_Options.visible_only, "esp", "visible_only", g_Options.visible_only);
	g_Config->PushItem(&g_Options.dormant, "esp", "dormant", g_Options.dormant);
	g_Config->PushItem(&g_Options.show_keybinds, "esp", "show_keybinds", g_Options.show_keybinds);
	g_Config->PushItem(&g_Options.flags, "esp", "flags", g_Options.flags);
	g_Config->PushItem(&g_Options.flags_scoped, "esp", "flags_scoped", g_Options.flags_scoped);
	g_Config->PushItem(&g_Options.flags_scoped_size, "esp", "flags_scoped_size", g_Options.flags_scoped_size);
	g_Config->PushItem(&g_Options.flags_reload, "esp", "flags_reload", g_Options.flags_reload);
	g_Config->PushItem(&g_Options.flags_reload_size, "esp", "flags_reload_size", g_Options.flags_reload_size);
	g_Config->PushItem(&g_Options.flags_bomb, "esp", "flags_bomb", g_Options.flags_bomb);
	g_Config->PushItem(&g_Options.flags_bomb_size, "esp", "flags_bomb_size", g_Options.flags_bomb_size);
	g_Config->PushItem(&g_Options.flags_flashed, "esp", "flags_flashed", g_Options.flags_flashed);
	g_Config->PushItem(&g_Options.flags_flashed_size, "esp", "flags_flashed_size", g_Options.flags_flashed_size);
	g_Config->PushItem(&g_Options.flags_hk, "esp", "flags_hk", g_Options.flags_hk);
	g_Config->PushItem(&g_Options.flags_hk_size, "esp", "flags_hk_size", g_Options.flags_hk_size);
	g_Config->PushItem(&g_Options.flags_defusing, "esp", "flags_defusing", g_Options.flags_defusing);
	g_Config->PushItem(&g_Options.flags_defusing_size, "esp", "flags_defusing_size", g_Options.flags_defusing_size);
	g_Config->PushItem(&g_Options.sound_esp, "esp", "soufnd_esp", g_Options.sound_esp);
	g_Config->PushItem(&g_Options.sound_esp_type, "esp", "sound_esp_type", g_Options.sound_esp_type);
	g_Config->PushItem(&g_Options.sound_esp_onteam, "esp", "sound_esp_onteam", g_Options.sound_esp_onteam);
	g_Config->PushItem(&g_Options.sound_esp_time, "esp", "sound_esp_time", g_Options.sound_esp_time);
	g_Config->PushItem(&g_Options.name_size, "esp", "name_size", g_Options.name_size);
	g_Config->PushItem(&g_Options.weapon_size, "esp", "weapon_size", g_Options.weapon_size);
	g_Config->PushItem(&g_Options.name_clr, "esp", "name_clr", g_Options.name_clr);
	g_Config->PushItem(&g_Options.weapon_clr, "esp", "weapon_clr", g_Options.weapon_clr);
	g_Config->PushItem(&g_Options.snipeline_clr, "esp", "snipeline_clr", g_Options.snipeline_clr);
	g_Config->PushItem(&g_Options.head_dot_clr, "esp", "head_dot_clr", g_Options.head_dot_clr);
	g_Config->PushItem(&g_Options.sound_esp_clr, "esp", "sound_esp_clr", g_Options.sound_esp_clr);
	g_Config->PushItem(&g_Options.hp_clr, "esp", "hp_clr", g_Options.hp_clr);
	g_Config->PushItem(&g_Options.armor_clr, "esp", "armor_clr", g_Options.armor_clr);
	g_Config->PushItem(&g_Options.ammobar_clr, "esp", "ammobar_clr", g_Options.ammobar_clr);
	// GLOW
	g_Config->PushItem(&g_Options.enabled_glow, "esp", "enabled_glow", g_Options.enabled_glow);
	g_Config->PushItem(&g_Options.players, "esp", "players", g_Options.players);
	g_Config->PushItem(&g_Options.on_chickens, "esp", "on_chickens", g_Options.on_chickens);
	g_Config->PushItem(&g_Options.on_team, "esp", "on_team", g_Options.on_team);
	g_Config->PushItem(&g_Options.c4_carrier, "esp", "c4_carrier", g_Options.c4_carrier);
	g_Config->PushItem(&g_Options.c4_planted, "esp", "c4_planted", g_Options.c4_planted);
	g_Config->PushItem(&g_Options.defuse_kits, "esp", "defuse_kits", g_Options.defuse_kits);
	g_Config->PushItem(&g_Options.weapons, "esp", "weapons", g_Options.weapons);
	g_Config->PushItem(&g_Options.type_glow, "esp", "type_glow", g_Options.type_glow);
	g_Config->PushItem(&g_Options.bloom_glow, "esp", "bloom_glow", g_Options.bloom_glow);
	g_Config->PushItem(&g_Options.enemy_clr, "esp", "enemy_clr", g_Options.enemy_clr);
	g_Config->PushItem(&g_Options.enemy_invis_clr, "esp", "enemy_invis_clr", g_Options.enemy_invis_clr);
	g_Config->PushItem(&g_Options.allies_clr, "esp", "allies_clr", g_Options.allies_clr);
	g_Config->PushItem(&g_Options.allies_invis_clr, "esp", "allies_invis_clr", g_Options.allies_invis_clr);
	g_Config->PushItem(&g_Options.chickens_clr, "esp", "chickens_clr", g_Options.chickens_clr);
	g_Config->PushItem(&g_Options.c4_carrier_clr, "esp", "c4_carrier_clr", g_Options.c4_carrier_clr);
	g_Config->PushItem(&g_Options.c4_planted_clr, "esp", "c4_planted_clr", g_Options.c4_planted_clr);
	g_Config->PushItem(&g_Options.defuse_kits_clr, "esp", "defuse_kits_clr", g_Options.defuse_kits_clr);
	g_Config->PushItem(&g_Options.weapons_clr, "esp", "weapons_clr", g_Options.weapons_clr);
	//CHAMS
	auto setupChams = [&](ConfChams& chams, std::string szName) -> void {
		g_Config->PushItem(&chams.DoNotRemoveModel, szName, "do_not_remove_model", chams.DoNotRemoveModel);
		for (size_t i = 0; i < chams.materials.size(); i++) {
			auto bName = szName + "_" + std::to_string(i);
			g_Config->PushItem(&chams.materials[i].blinking, bName, "pulsing", chams.materials[i].blinking);
			g_Config->PushItem(&chams.materials[i].color, bName, "clr", chams.materials[i].color);
			g_Config->PushItem(&chams.materials[i].enabled, bName, "enabled", chams.materials[i].enabled);
			g_Config->PushItem(&chams.materials[i].healthBased, bName, "hBased", chams.materials[i].healthBased);
			g_Config->PushItem(&chams.materials[i].material, bName, "materialIndex", chams.materials[i].material);
			g_Config->PushItem(&chams.materials[i].wireframe, bName, "wireframe", chams.materials[i].wireframe);
		}
	};
	setupChams(g_Chams->chams_player_allies_all, "chams_player_allies_all");
	setupChams(g_Chams->chams_player_allies_visible, "chams_player_allies_visible");
	setupChams(g_Chams->chams_player_allies_occluded, "chams_player_allies_occluded");

	setupChams(g_Chams->chams_player_enemies_all, "chams_player_enemies_all");
	setupChams(g_Chams->chams_player_enemies_visible, "chams_player_enemies_visible");
	setupChams(g_Chams->chams_player_enemies_occluded, "chams_player_enemies_occluded");

	setupChams(g_Chams->chams_player_backtrack, "chams_player_backtrack");

	setupChams(g_Chams->chams_player_local_player, "chams_player_local_player");
	setupChams(g_Chams->chams_weapons, "chams_weapons");
	setupChams(g_Chams->chams_sleeves, "chams_sleeves");
	setupChams(g_Chams->chams_hands, "chams_hands");

	//MISC
	g_Config->PushItem(g_Options.misc_bhop, "misc", "misc_bhop", g_Options.misc_bhop);
	g_Config->PushItem(g_Options.viewmodel_fov, "misc", "viewmodel_fov", g_Options.viewmodel_fov);
	g_Config->PushItem(g_Options.misc_watermark, "misc", "misc_watermark", g_Options.misc_watermark);
	g_Config->PushItem(&g_Options.misc_spectators, "misc", "misc_spectators", g_Options.misc_spectators);
	g_Config->PushItem(&g_Options.bloom_value, "misc", "bloom_value", g_Options.bloom_value);
	g_Config->PushItem(&g_Options.model_ambient_value, "misc", "model_ambient_value", g_Options.model_ambient_value);


	g_Config->PushItem(&g_Options.accent_color, "esp", "accent_color", g_Options.accent_color);
	g_Config->PushItem(&g_Visuals->options.third_person, "esp", "third_person", g_Visuals->options.third_person);
	g_Config->PushItem(&g_Visuals->options.third_person_bind, "esp", "third_person_bind", g_Visuals->options.third_person_bind);
	g_Config->PushItem(&g_Visuals->options.third_person_dist, "misc", "third_person_dist", g_Visuals->options.third_person_dist);
}


using nlohmann::json;
void Config::read(std::string path) {
	std::filesystem::create_directory("c:\\CSGOSimple\\");
	path = "c:\\CSGOSimple\\" + path + ".cfg";
	if (!file_exists(path)) return;

	json retData;
	std::ifstream reader(path);

	json data;
	reader >> data;

	reader.close();

	auto sanityChecks = [](json& d, std::string& c, std::string& n) -> bool {
		return d.find(c) != d.end() && d[c].find(n) != d[c].end();
	};

	for (auto& b : booleans) 
		if (sanityChecks(data, b.category, b.name))  
			*b.pointer = data[b.category][b.name].get<bool>();
	for (auto& i : ints) 
		if (sanityChecks(data, i.category, i.name))  
			*i.pointer = data[i.category][i.name].get<int>();
	for (auto& f : floats) 
		if (sanityChecks(data, f.category, f.name))  
			*f.pointer = data[f.category][f.name].get<float>();
	for (auto& c : colors) {
		if (sanityChecks(data, c.category, c.name)) {
			auto elem = data[c.category][c.name];
			c.pointer->SetColor(elem["r"].get<int>(), elem["g"].get<int>(), elem["b"].get<int>(), elem["a"].get<int>());
			if (elem.find("rainbow") != elem.end())
				c.pointer->rainbow = elem["rainbow"].get<bool>();
		}
	}
	for (auto& k : keybinds)
		if (sanityChecks(data, k.category, k.name)) 
			k.pointer->Deserialize(data[k.category][k.name]);

	g_CustomWeaponGroups->LoadSettings(data);
	Globals::UpdateNightMode = true;
}

void Config::save(std::string path) {
	std::filesystem::create_directory("c:\\CSGOSimple\\");
	path = "c:\\CSGOSimple\\" + path + ".cfg";

	json retData;
	
	auto sanityChecks = [&](std::string category) -> void {
		if (retData.find(category) == retData.end())
			retData[category] = json();
	};

	for (auto& b : booleans) {
		sanityChecks(b.category);
		retData[b.category][b.name] = *b.pointer;
	}
	for (auto& i : ints) {
		sanityChecks(i.category);
		retData[i.category][i.name] = *i.pointer;
	}
	for (auto& f : floats) {
		sanityChecks(f.category);
		retData[f.category][f.name] = *f.pointer;
	}
	for (auto& c : colors) {
		sanityChecks(c.category);
		json d;
		Color clr = *c.pointer;
		d["r"] = clr.r();
		d["g"] = clr.g();
		d["b"] = clr.b();
		d["a"] = clr.a();
		d["rainbow"] = clr.rainbow;
		retData[c.category][c.name] = d;
	}
	for (auto& k : keybinds) {
		sanityChecks(k.category);
		retData[k.category][k.name] = k.pointer->Serialize();
	}

	g_CustomWeaponGroups->SaveSettings(retData);

	std::ofstream reader(path);
	reader.clear();
	reader << std::setw(4) << retData << std::endl;
	reader.close();
}

void Config::refresh() {
	config_files.clear();
	std::filesystem::create_directory("c:\\CSGOSimple\\");

	for (const auto& p : std::filesystem::recursive_directory_iterator("c:\\CSGOSimple\\")) {
		if (!std::filesystem::is_directory(p) &&
			p.path().extension().string() == ".cfg") {
			auto file = p.path().filename().string();
			config_files.push_back(file.substr(0, file.size() - 4));
		}
	}
}

void Config::remove(std::string path) {
	std::filesystem::create_directory("c:\\CSGOSimple\\");
	path = "c:\\CSGOSimple\\" + path + ".cfg";

	std::filesystem::remove(path);
	refresh();
}

void Config::Menu() {
	static std::string selected_cfg = "";
	static char cfg_name[32];

	ImGui::Columns(2, nullptr, false);

	ImGui::Text("Selector:");
	ImGui::BeginChild("##list", ImVec2(-1, -1), true); {
		if (ImGui::InputText("File", cfg_name, 32)) selected_cfg = std::string(cfg_name);

		ImGui::ListBoxHeader("##cfglist", ImVec2(-1, -1));

		for (auto cfg : config_files)
			if (ImGui::Selectable(cfg.c_str(), cfg == selected_cfg))
				selected_cfg = cfg;

		ImGui::ListBoxFooter();
	};
	ImGui::EndChild();
	ImGui::NextColumn();

	ImGui::Text("Controls:");
	ImGui::BeginChild("##second", ImVec2(-1, -1), true); {
		if (ImGui::Button("Load", ImVec2(-1, 19))) {
			read(selected_cfg);
			refresh();
		}
		if (ImGui::Button("Save", ImVec2(-1, 19))) {
			save(selected_cfg);
			refresh();
		}
		if (ImGui::Button("Delete", ImVec2(-1, 19))) {
			remove(selected_cfg);
			refresh();
		}

		if (ImGui::Button("Refresh list", ImVec2(-1, 19)))
			refresh();
		if (ImGui::Button("Reset settings", ImVec2(-1, 19))) {
			reset();
		}
	};
	ImGui::EndChild();
}

void Config::reset() {
	for (auto& b : booleans)
		*b.pointer = b.default_value;
	for (auto& i : ints)
		*i.pointer = i.default_value;
	for (auto& c : colors)
		*c.pointer = c.default_value;
	for (auto& f : floats)
		*f.pointer = f.default_value;
	for (auto& k : keybinds)
		k.pointer->Deserialize(k.default_value.Serialize()); 

	g_CustomWeaponGroups->FillDefaults();
}

void Config::ApplyRainbow() {
	for (auto& clr : colors) {
		if (clr.pointer->rainbow) {
			clr.pointer->UpdateRainbow(0.99f, 1.f);
		}
	}
}

Config* g_Config = new Config();