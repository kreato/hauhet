#include "visuals.hpp"
#include "../options.hpp"
#include "../config.hpp"
#include "../menu/menu.hpp"
#include "../sdk/render/render.hpp"
#include "chams.hpp"


void c_visuals::Run() {
	if (!g_LocalPlayer) return;

	for (int i = 1; i <= g_GlobalVars->maxClients; i++) {
		auto entity = (C_BasePlayer*)C_BasePlayer::GetPlayerByIndex(i);

		if (!entity) continue;


		const int fade = (int)((3.4444444447f * g_GlobalVars->frametime) * 255);

		auto new_alpha = EspAlphas[i];
		new_alpha += entity->IsDormant() ? -fade : fade;

		if (new_alpha > (entity->m_bGunGameImmunity() ? 130 : 210))
			new_alpha = (entity->m_bGunGameImmunity() ? 130 : 210);
		auto alpha_divider = g_Options.dormant ? 50 : 0;
		if (new_alpha < alpha_divider)
			new_alpha = alpha_divider;

		EspAlphas[i] = new_alpha;

		visuals_player player(entity, new_alpha);
		if (player.IsValid()) player.Draw();
		LastDormant[i] = entity->IsDormant();
	}

	if (g_Options.sound_esp) DrawSounds();
}


void c_visuals::ThirdPerson() {
	if (!g_LocalPlayer)
		return;

	if (g_LocalPlayer->IsAlive() && g_Options.misc_thirdperson || GetKeyState(g_Options.misc_thirdperson_hotkey))
	{
		if (!g_Input->m_fCameraInThirdPerson)
		{
			g_Input->m_fCameraInThirdPerson = true;
		}

		float dist = 150.f;

		QAngle* view = g_LocalPlayer->GetVAngles();
		trace_t tr;
		Ray_t ray;

		Vector desiredCamOffset = Vector(cos(DEG2RAD(view->yaw)) * dist,
			sin(DEG2RAD(view->yaw)) * dist,
			sin(DEG2RAD(-view->pitch)) * dist
		);

		//cast a ray from the Current camera Origin to the Desired 3rd person Camera origin
		ray.Init(g_LocalPlayer->GetEyePos(), (g_LocalPlayer->GetEyePos() - desiredCamOffset));
		CTraceFilter traceFilter;
		traceFilter.pSkip = g_LocalPlayer;
		g_EngineTrace->TraceRay(ray, MASK_SHOT, &traceFilter, &tr);

		Vector diff = g_LocalPlayer->GetEyePos() - tr.endpos;

		float distance2D = sqrt(abs(diff.x * diff.x) + abs(diff.y * diff.y));// Pythagorean

		bool horOK = distance2D > (dist - 2.0f);
		bool vertOK = (abs(diff.z) - abs(desiredCamOffset.z) < 3.0f);

		float cameraDistance;

		if (horOK && vertOK)  // If we are clear of obstacles
		{
			cameraDistance = dist; // go ahead and set the distance to the setting
		}
		else
		{
			if (vertOK) // if the Vertical Axis is OK
			{
				cameraDistance = distance2D * 0.95f;
			}
			else// otherwise we need to move closer to not go into the floor/ceiling
			{
				cameraDistance = abs(diff.z) * 0.95f;
			}
		}
		g_Input->m_fCameraInThirdPerson = true;

		g_Input->m_vecCameraOffset.z = cameraDistance;
	}
	else
	{
		g_Input->m_fCameraInThirdPerson = false;
	}
}



void c_visuals::Bloom() {
	if (!g_EngineClient->IsConnected() || !g_EngineClient->IsInGame())
		return;

	static auto bloom = g_CVar->FindVar("mat_bloom_scalefactor_scalar");
	bloom->SetValue(g_Options.bloom_value);

	static auto bloom_scale = g_CVar->FindVar("mat_bloomamount_rate");
	bloom_scale->SetValue(5.05f / 255.f);

	static auto ambient = g_CVar->FindVar("r_modelAmbientMin");
	ambient->SetValue(g_Options.model_ambient_value);
}

void c_visuals::DrawSounds() {
	UpdateSounds();

	auto Add3DCircle = [](const Vector& position, Color color, float radius) {
		float precision = 160.0f;

		const float step = DirectX::XM_2PI / precision;

		for (float a = 0.f; a < DirectX::XM_2PI; a += step) {
			Vector start(radius * cosf(a) + position.x, radius * sinf(a) + position.y, position.z);
			Vector end(radius * cosf(a + step) + position.x, radius * sinf(a + step) + position.y, position.z);

			Vector start2d, end2d;
			if (!Math::WorldToScreen(start, start2d) || !Math::WorldToScreen(end, end2d))
				return;

			Render::Get().RenderLine(start2d.x, start2d.y, end2d.x, end2d.y, color);
		}
	};


	for (auto& [entIndex, sound] : m_Sounds) {
		if (sound.empty())
			continue;

		for (auto& info : sound) {
			if (info.soundTime + g_Options.sound_esp_time < g_GlobalVars->realtime)
				info.alpha -= g_GlobalVars->frametime;

			if (info.alpha <= 0.0f)
				continue;

			float deltaTime = g_GlobalVars->realtime - info.soundTime;

			auto factor = deltaTime / g_Options.sound_esp_time;
			if (factor > 1.0f)
				factor = 1.0f;

			float radius = g_Options.sound_esp_radius * factor;

			switch (g_Options.sound_esp_type) {
			case 0:
				Add3DCircle(info.soundPos, g_Options.sound_esp_clr, radius);
				break;
			case 1:
			{
				static float lasttime[64] = { 0.f };

				if (g_GlobalVars->curtime - lasttime[entIndex] > .5f) { // pFix
					BeamInfo_t beamInfo;
					beamInfo.m_nType = TE_BEAMRINGPOINT;
					beamInfo.m_pszModelName = "sprites/purplelaser1.vmt";
					beamInfo.m_nModelIndex = g_MdlInfo->GetModelIndex("sprites/purplelaser1.vmt");
					beamInfo.m_nHaloIndex = -1;
					beamInfo.m_flHaloScale = 5;
					beamInfo.m_flLife = 1.f; //
					beamInfo.m_flWidth = 10.f;
					beamInfo.m_flFadeLength = 1.0f;
					beamInfo.m_flAmplitude = 0.f;
					beamInfo.m_flRed = g_Options.sound_esp_clr.r();
					beamInfo.m_flGreen = g_Options.sound_esp_clr.g();
					beamInfo.m_flBlue = g_Options.sound_esp_clr.b();
					beamInfo.m_flBrightness = g_Options.sound_esp_clr.a();
					beamInfo.m_flSpeed = 0.f;
					beamInfo.m_nStartFrame = 0.f;
					beamInfo.m_flFrameRate = 60.f;
					beamInfo.m_nSegments = -1;
					beamInfo.m_nFlags = FBEAM_FADEOUT;
					beamInfo.m_vecCenter = info.soundPos + Vector(0, 0, 5);
					beamInfo.m_flStartRadius = 20.f;
					beamInfo.m_flEndRadius = 640.f;
					auto myBeam = g_ViewRenderBeams->CreateBeamRingPoint(beamInfo);

					if (myBeam)
						g_ViewRenderBeams->DrawBeam(myBeam);

					lasttime[entIndex] = g_GlobalVars->curtime;
				}
				else
					info.alpha = 0.f;
			}
			break;
			}
		}

		while (!sound.empty()) {
			auto& back = sound.back();
			if (back.alpha <= 0.0f)
				sound.pop_back();
			else
				break;
		}
	}
}

void c_visuals::molotov_timer() {
	if (!g_EngineClient->IsInGame() || !g_EngineClient->IsConnected())
		return;

	for (auto i = 0; i < g_EntityList->GetHighestEntityIndex(); i++)
	{
		auto ent = C_BaseEntity::GetEntityByIndex(i);
		if (!ent)
			continue;

		if (ent->GetClientClass()->m_ClassID != ClassId_CInferno)
			continue;

		auto inferno = reinterpret_cast<Inferno_t*>(ent);

		auto origin = inferno->m_vecOrigin();
		auto screen_origin = Vector();

		if (!Math::WorldToScreen(origin, screen_origin))
			return;

		const auto spawn_time = inferno->GetSpawnTime();
		const auto timer = (spawn_time + Inferno_t::GetExpireTime()) - g_GlobalVars->curtime;
		const auto l_spawn_time = *(float*)(uintptr_t(inferno) + 0x20);
		const auto l_factor = ((l_spawn_time + 7.03125f) - g_GlobalVars->curtime) / 7.03125f;

		Render::Get().RenderBoxFilled(screen_origin.x - 49, screen_origin.y + 10, (screen_origin.x - 49) + 98.f, (screen_origin.y + 10) + 4.f, Color::Black);
		Render::Get().RenderBoxFilled(screen_origin.x - 49, screen_origin.y + 10, (screen_origin.x - 49) + (98.f * l_factor), (screen_origin.y + 10) + 4.f, Color::Red);

		Render::Get().RenderText(std::to_string((int)(round(timer + 1))) + "s", screen_origin.x, screen_origin.y - 5.f, 18.f, g_Options.molotov_timer_color);
		Render::Get().RenderCircle3D(origin, 50, 150.f, g_Options.molotov_timer_color);
	}
}

void c_visuals::nightmode() {
	static auto r_modelAmbientMin = g_CVar->FindVar("r_modelAmbientMin");
	static auto mat_force_tonemap_scale = g_CVar->FindVar("mat_force_tonemap_scale");

	r_modelAmbientMin->SetValue(g_Options.nightmode_value > 1.f ? 1.f : 0.0f);
	mat_force_tonemap_scale->SetValue(g_Options.nightmode_value);
}

void c_visuals::nosmoke() {
	static auto linegoesthrusmoke = Utils::PatternScan(GetModuleHandleA("client.dll"), "A3 ? ? ? ? 57 8B CB");
	static bool set = true;
	std::vector<const char*> vistasmoke_wireframe =
	{
		"particle/vistasmokev1/vistasmokev1_smokegrenade",
	};

	std::vector<const char*> vistasmoke_nodraw =
	{
		"particle/vistasmokev1/vistasmokev1_fire",
		"particle/vistasmokev1/vistasmokev1_emods",
		"particle/vistasmokev1/vistasmokev1_emods_impactdust",
	};

	if (!g_Options.nosmoke)
	{
		if (set)
		{
			for (auto material_name : vistasmoke_wireframe)
			{
				IMaterial* mat = g_MatSystem->FindMaterial(material_name, TEXTURE_GROUP_OTHER);
				mat->SetMaterialVarFlag(MATERIAL_VAR_WIREFRAME, false);
			}
			for (auto material_name : vistasmoke_nodraw)
			{
				IMaterial* mat = g_MatSystem->FindMaterial(material_name, TEXTURE_GROUP_OTHER);
				mat->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, false);
			}
			set = false;
		}
		return;
	}

	set = true;

	for (auto mat_s : vistasmoke_wireframe)
	{
		IMaterial* mat = g_MatSystem->FindMaterial(mat_s, TEXTURE_GROUP_OTHER);
		mat->SetMaterialVarFlag(MATERIAL_VAR_WIREFRAME, true);
	}

	for (auto mat_n : vistasmoke_nodraw)
	{
		IMaterial* mat = g_MatSystem->FindMaterial(mat_n, TEXTURE_GROUP_OTHER);
		mat->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
	}

	static auto smokecout = *(DWORD*)(linegoesthrusmoke + 0x1);
	*(int*)(smokecout) = 0;

}

void c_visuals::no_visual_recoil(CViewSetup* pSetup) {
	if (!g_Options.no_visual_recoil || !g_EngineClient->IsInGame())
		return;

	QAngle viewPunch = g_LocalPlayer->m_viewPunchAngle();
	QAngle aimPunch = g_LocalPlayer->m_aimPunchAngle();

	pSetup->angles[0] -= (viewPunch[0] + (aimPunch[0] * 2 * 0.4499999f));
	pSetup->angles[1] -= (viewPunch[1] + (aimPunch[1] * 2 * 0.4499999f));
	pSetup->angles[2] -= (viewPunch[2] + (aimPunch[2] * 2 * 0.4499999f));
}

void c_visuals::UpdateSounds() {
	sounds.RemoveAll();
	g_EngineSound->GetActiveSounds(sounds);
	if (sounds.Count() < 1)
		return;

	Vector eye_pos = g_LocalPlayer->GetEyePos();
	for (int i = 0; i < sounds.Count(); ++i) {
		SndInfo_t& sound = sounds.Element(i);
		if (sound.m_nSoundSource < 1)
			continue;

		C_BasePlayer* player = C_BasePlayer::GetPlayerByIndex(sound.m_nSoundSource);
		if (!player)
			continue;

		if (player->m_hOwnerEntity().IsValid() && player->IsWeapon()) {
			player = (C_BasePlayer*)player->m_hOwnerEntity().Get();
		}

		if (!player->IsPlayer() || !player->IsAlive())
			continue;

		if ((C_BasePlayer*)g_LocalPlayer == player)
			continue;

		if (!g_Options.sound_esp_onteam && player->m_iTeamNum() == g_LocalPlayer->m_iTeamNum()) return;

		if (player->m_vecOrigin().DistTo(g_LocalPlayer->m_vecOrigin()) > 900)
			continue;

		auto& player_sound = m_Sounds[player->EntIndex()];
		if (player_sound.size() > 0) {
			bool should_break = false;
			for (const auto& snd : player_sound) {
				if (snd.guid == sound.m_nGuid) {
					should_break = true;
					break;
				}
			}

			if (should_break)
				continue;
		}

		SoundInfo_t& snd = player_sound.emplace_back();
		snd.guid = sound.m_nGuid;
		snd.soundPos = *sound.m_pOrigin;
		snd.soundTime = g_GlobalVars->realtime;
		snd.alpha = 1.0f;
	}
}

c_visuals* g_Visuals = new c_visuals();