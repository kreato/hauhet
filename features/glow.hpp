#pragma once
#include "../sdk/sdk.hpp"
#include "../sdk/csgostructs.hpp"


class Glow {
public:
    Glow();
    ~Glow();

public:
    void Run();
    void Shutdown();
};


extern Glow* g_Glow;