#pragma once
#include "../singleton.hpp"
#include "../sdk/sdk.hpp"
#include "../sdk/csgostructs.hpp"
#include "../helpers/math.hpp"
#include "../config.hpp"
#include "glow.hpp"
#include <d3dx9.h>
#include <d3d9.h>
#include <map>

struct PlayersPreviousInfo_t {
	int hp = -1;
	int hpDifference = 0;
	float hpDiffTime = 0.f;

	int armor = -1;
	int armorDifference = 0;
	float armorDiffTime = 0.f;
};

class c_visuals {
private:
	struct sideinfo_t {
		std::string str;
		Color color;
		float size;
	}; 
	struct SoundInfo_t {
		int guid;
		float soundTime;
		float alpha;
		Vector soundPos;
	};
private:
	class visuals_player {
	private:
		int alpha = 255;
		bool is_visible = false;
		bool in_team = false;
		int32_t health = 75;
		int armor = 75;
		std::string playerName = "";
		std::string weaponName = "";
		C_BasePlayer* player = nullptr;
		RECT bbox{ 0,0,0,0 };
		Vector head_pos, feet_pos;
		PlayersPreviousInfo_t* previousInfo;
	private:
		void DrawBoxes();
		void DrawSkeleton();
		void DrawHP();
		void DrawName();
		void DrawArmor();
		void DrawWeapon();
		void DrawSnipeLine();
		void DrawHeadDot();
		void DrawFlags();
		void DrawAmmoBar();
	private:
		void ImplementAlpha(Color& clr) {
			if ((int)clr.a() > alpha)
				clr.SetColor(clr.r(), clr.g(), clr.b(), alpha);
		}
	public:
		visuals_player(C_BasePlayer* player, int alpha);
		bool IsValid();
		bool Draw();
		void Initialize();
	private:
		RECT GetBBox(C_BaseEntity* ent) {
			RECT rect = RECT();
			ICollideable* collideable = ent->GetCollideable();
			if (!collideable) return rect;

			Vector min = collideable->OBBMins();
			Vector max = collideable->OBBMaxs();

			Vector points[] = {
				Vector(min.x, min.y, min.z),
				Vector(min.x, max.y, min.z),
				Vector(max.x, max.y, min.z),
				Vector(max.x, min.y, min.z),
				Vector(max.x, max.y, max.z),
				Vector(min.x, max.y, max.z),
				Vector(min.x, min.y, max.z),
				Vector(max.x, min.y, max.z) };

			auto trans = ent->m_rgflCoordinateFrame();

			Vector pointsTransformed[8];
			Vector screen_points[8];

			bool init = false;

			for (int i = 0; i < 8; i++)
			{
				Math::VectorTransform(points[i], trans, pointsTransformed[i]);

				if (!Math::WorldToScreen(pointsTransformed[i], screen_points[i]))
					return rect;

				if (!init) {
					rect.left = screen_points[0].x;
					rect.top = screen_points[0].y;
					rect.right = screen_points[0].x;
					rect.bottom = screen_points[0].y;
					init = true;
				}
				else {
					if (rect.left > screen_points[i].x)
						rect.left = screen_points[i].x;
					if (rect.bottom < screen_points[i].y)
						rect.bottom = screen_points[i].y;
					if (rect.right < screen_points[i].x)
						rect.right = screen_points[i].x;
					if (rect.top > screen_points[i].y)
						rect.top = screen_points[i].y;
				}
			}

			return rect;
		}
	};
private:
	int EspAlphas[65];
	bool LastDormant[65];
	IDirect3DTexture9* PreviewModel = nullptr;
	std::map< int, std::vector< SoundInfo_t > > m_Sounds;
	CUtlVector<SndInfo_t> sounds;
private:
	void UpdateSounds();
	void DrawSounds();
public:
	void Run();
	void VelocityInfo();
	void Bloom();
public:
	PlayersPreviousInfo_t PreviousInfos[4096];
	void ThirdPerson();
	void molotov_timer();
	void nightmode();
	void nosmoke();
	void no_visual_recoil(CViewSetup* pSetup);
private:
	struct misc_options_t {
		bool third_person = false;
		bool third_person_pres = false;
		CKeyBind third_person_bind = CKeyBind(&third_person_pres, &third_person, "Third Person");
		float third_person_dist = 50.f;
	};
public:
	misc_options_t options;
};

extern c_visuals* g_Visuals;