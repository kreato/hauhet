#include "glow.hpp"
#include "visuals.hpp"

Glow* g_Glow = new Glow();

Glow::Glow()
{
}

Glow::~Glow()
{

}

void Glow::Shutdown()
{
    // Remove glow from all entities
    for (auto i = 0; i < g_GlowObjManager->m_GlowObjectDefinitions.Count(); i++) {
        auto& glowObject = g_GlowObjManager->m_GlowObjectDefinitions[i];
        auto entity = reinterpret_cast<C_BasePlayer*>(glowObject.m_pEntity);

        if (glowObject.IsUnused())
            continue;

        if (!entity || entity->IsDormant())
            continue;

        glowObject.m_flAlpha = 0.0f;
    }
}

void Glow::Run() // TODO: Fix that shit and support more targets
{
    for (auto i = 0; i < g_GlowObjManager->m_GlowObjectDefinitions.Count(); i++) {
        auto& glowObject = g_GlowObjManager->m_GlowObjectDefinitions[i];
        auto entity = reinterpret_cast<C_BasePlayer*>(glowObject.m_pEntity);

        if (glowObject.IsUnused())
            continue;

        if (!entity || entity->IsDormant())
            continue;

        auto class_id = entity->GetClientClass()->m_ClassID;
        auto color = Color(255, 255, 255);

        switch (class_id) {
        case ClassId_CCSPlayer:
        {
            auto is_enemy = entity->m_iTeamNum() != g_LocalPlayer->m_iTeamNum();

            if (entity->HasC4() && is_enemy && g_Options.c4_carrier) {
                color = g_Options.c4_carrier_clr;
                break;
            }

            if (!g_Options.players || !entity->IsAlive())
                continue;

            if (!is_enemy && !g_Options.on_team)
                continue;

            if (!g_LocalPlayer->CanSeePlayer(entity, entity->GetEyePos())) {
                if (is_enemy) {
                    color.SetColor(g_Options.enemy_invis_clr.r(), g_Options.enemy_invis_clr.g(), g_Options.enemy_invis_clr.b(), g_Options.enemy_invis_clr.a());
                }
                else {
                    color.SetColor(g_Options.allies_invis_clr.r(), g_Options.allies_invis_clr.g(), g_Options.allies_invis_clr.b(), g_Options.allies_invis_clr.a());
                }
            }
            else {
                if (is_enemy) {
                    color.SetColor(g_Options.enemy_clr.r(), g_Options.enemy_clr.g(), g_Options.enemy_clr.b(), g_Options.enemy_clr.a());
                }
                else {
                    color.SetColor(g_Options.allies_clr.r(), g_Options.allies_clr.g(), g_Options.allies_clr.b(), g_Options.allies_clr.a());
                }
            }
            break;
        }
        case ClassId_CChicken:
            if (!g_Options.on_chickens)
                continue;
            entity->m_bShouldGlow() = true;
            color.SetColor(g_Options.chickens_clr.r(), g_Options.chickens_clr.g(), g_Options.chickens_clr.b(), g_Options.chickens_clr.a());
            break;
        case ClassId_CBaseAnimating:
            if (!g_Options.defuse_kits)
                continue;
            color.SetColor(g_Options.defuse_kits_clr.r(), g_Options.defuse_kits_clr.g(), g_Options.defuse_kits_clr.b(), g_Options.defuse_kits_clr.a());
            break;
        case ClassId_CPlantedC4:
            if (!g_Options.c4_planted)
                continue;
            color.SetColor(g_Options.c4_planted_clr.r(), g_Options.c4_planted_clr.g(), g_Options.c4_planted_clr.b(), g_Options.c4_planted_clr.a());
            break;
        default:
        {
            if (entity->IsWeapon()) {
                if (!g_Options.weapons)
                    continue;
                color.SetColor(g_Options.weapons_clr.r(), g_Options.weapons_clr.g(), g_Options.weapons_clr.b(), g_Options.weapons_clr.a());
            }
            else
                continue;
        }
        }

        glowObject.m_flRed = color.r() / 255.0f;
        glowObject.m_flGreen = color.g() / 255.0f;
        glowObject.m_flBlue = color.b() / 255.0f;
        glowObject.m_flAlpha = color.a() / 255.f;
        glowObject.m_bRenderWhenOccluded = true;
        glowObject.m_bRenderWhenUnoccluded = false;
        glowObject.m_nGlowStyle = g_Options.type_glow;
        glowObject.m_bFullBloomRender = g_Options.bloom_glow;
    }
}