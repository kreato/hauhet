#include "aimbot.hpp"
#include "../helpers/math.hpp"
#include "../helpers/input.hpp"
#include "../menu/menu.hpp"
#include "../config.hpp"
#include "autowall.hpp"
#include "weapon_groups.hpp"
#include <cmath>
#include <random>
#include "../helpers/utils.hpp"

//--------------------------------------------------------------------------------
bool Aimbot::IsRcs() {
	return g_LocalPlayer->m_iShotsFired() >= settings.rcs_start + 1;
}
//--------------------------------------------------------------------------------
float GetRealDistanceFOV(float distance, QAngle angle, CUserCmd* cmd)
{
	Vector aimingAt;
	Math::AngleVectors(cmd->viewangles, aimingAt);
	aimingAt *= distance;
	Vector aimAt;
	Math::AngleVectors(angle, aimAt);
	aimAt *= distance;
	return aimingAt.DistTo(aimAt) / 5;
}
//--------------------------------------------------------------------------------
float Aimbot::GetFovToPlayer(QAngle viewAngle, QAngle aimAngle)
{
	QAngle delta = aimAngle - viewAngle;
	Math::FixAngles(delta);
	return sqrtf(powf(delta.pitch, 2.0f) + powf(delta.yaw, 2.0f));
}
//--------------------------------------------------------------------------------
bool Aimbot::IsLineGoesThroughSmoke(Vector vStartPos, Vector vEndPos) {
	static auto LineGoesThroughSmokeFn = (bool(*)(Vector vStartPos, Vector vEndPos))Utils::PatternScan(GetModuleHandleA("client.dll"), "55 8B EC 83 EC 08 8B 15 ? ? ? ? 0F 57 C0");
	return LineGoesThroughSmokeFn(vStartPos, vEndPos);
}
//--------------------------------------------------------------------------------
bool Aimbot::IsEnabled(CUserCmd* pCmd) {

	if (!g_Options.aimbot_enable || !g_EngineClient->IsConnected() || !g_LocalPlayer || !g_LocalPlayer->IsAlive()) {
		return false;
	}

	auto pWeapon = g_LocalPlayer->m_hActiveWeapon();
	if (!pWeapon || !pWeapon->GetCSWeaponData() || !pWeapon->IsGun()) {
		return false;
	}

	if ((pWeapon->m_Item().m_iItemDefinitionIndex() == WEAPON_AWP || pWeapon->m_Item().m_iItemDefinitionIndex() == WEAPON_SSG08) && settings.only_in_zoom && !g_LocalPlayer->m_bIsScoped()) {
		return false;
	}

	if (!pWeapon->HasBullets() || pWeapon->IsReloading() || !pWeapon->CanFire() || Menu::Get().IsVisible()) {
		return false;
	}

	if (g_Options.aimbot_on_key && g_Options.aimbot_key_pressed) return true;

	return pCmd->buttons & IN_ATTACK;
}
//bool Aimbot::IsEnabled(CUserCmd* pCmd)
//{
//	if (!g_EngineClient->IsConnected() || !g_LocalPlayer || !g_LocalPlayer->IsAlive()) {
//		return false;
//	}
//	auto pWeapon = g_LocalPlayer->m_hActiveWeapon();
//	if (!pWeapon || !(pWeapon->IsSniper() || pWeapon->IsPistol() || pWeapon->IsRifle())) {
//		return false;
//	}
//
//	if (!g_Options.aimbot_enable || !(pCmd->buttons & IN_ATTACK)) {
//		return false;
//	}
//	if ((pWeapon->m_Item().m_iItemDefinitionIndex() == WEAPON_AWP || pWeapon->m_Item().m_iItemDefinitionIndex() == WEAPON_SSG08) && settings.only_in_zoom && !g_LocalPlayer->m_bIsScoped()) {
//		return false;
//	}
//	if (settings.fov == 0 && settings.silent_fov == 0 && !settings.rcs) {
//		return false;
//	}
//	if (!pWeapon->HasBullets() || pWeapon->IsReloading()) {
//		return false;
//	}
//	return true;
//}
//--------------------------------------------------------------------------------
float Aimbot::GetSmooth()
{
	float smooth = IsRcs() && settings.rcs_smooth_enabled ? settings.rcs_smooth : settings.smooth;
	if (settings.humanize) {
		smooth += RandomFloat(-1, 4);
	}
	return smooth;
}
//--------------------------------------------------------------------------------
void Aimbot::Smooth(QAngle currentAngle, QAngle aimAngle, QAngle& angle)
{
	if (GetSmooth() <= 1) {
		return;
	}
	Vector vAimAngle;
	Math::AngleVectors(aimAngle, vAimAngle);
	Vector vCurrentAngle;
	Math::AngleVectors(currentAngle, vCurrentAngle);
	Vector delta = vAimAngle - vCurrentAngle;
	Vector smoothed = vCurrentAngle + delta / GetSmooth();
	Math::VectorAngles(smoothed, angle);
}
//--------------------------------------------------------------------------------
void Aimbot::RCS(QAngle& angle, C_BasePlayer* target)
{
	if (!settings.rcs || !IsRcs()) {
		return;
	}
	if (settings.rcs_x == 0 && settings.rcs_y == 0) {
		return;
	}
	if (target) {
		QAngle punch = g_LocalPlayer->m_aimPunchAngle();
		angle.pitch -= punch.pitch * (settings.rcs_x / 50.f);
		angle.yaw -= punch.yaw * (settings.rcs_y / 50.f);
	}
	else if (settings.rcs_type == 0) {
		QAngle NewPunch = { CurrentPunch.pitch - RCSLastPunch.pitch, CurrentPunch.yaw - RCSLastPunch.yaw, 0 };
		angle.pitch -= NewPunch.pitch * (settings.rcs_x / 50.f);
		angle.yaw -= NewPunch.yaw * (settings.rcs_y / 50.f);
	}
	Math::FixAngles(angle);
}
//--------------------------------------------------------------------------------
float Aimbot::GetFov()
{
	if (IsRcs() && settings.rcs && settings.rcs_fov_enabled) return settings.rcs_fov;
	if (!silent_enabled) return settings.fov;
	return settings.silent_fov > settings.fov ? settings.silent_fov : settings.fov;
}
//--------------------------------------------------------------------------------
//C_BasePlayer* Aimbot::GetClosestPlayer(CUserCmd* cmd, int& bestBone)
//{
//	QAngle ang;
//	Vector eVecTarget;
//	Vector pVecTarget = g_LocalPlayer->GetEyePos();
//	if (target && !kill_delay && settings.kill_delay > 0 && target->IsNotTarget()) {
//		target = NULL;
//		shot_delay = false;
//		kill_delay = true;
//		kill_delay_time = (int)GetTickCount() + settings.kill_delay;
//	}
//	if (kill_delay) {
//		if (kill_delay_time <= (int)GetTickCount()) kill_delay = false;
//		else return NULL;
//	}
//	C_BasePlayer* player;
//	target = NULL;
//	int bestHealth = 100.f;
//	float bestFov = 9999.f;
//	float bestDamage = 0.f;
//	float bestBoneFov = 9999.f;
//	float bestDistance = 9999.f;
//	int health;
//	float fov;
//	float damage;
//	float distance;
//	int fromBone = settings.aim_type == 1 ? 0 : settings.hitbox;
//	int toBone = settings.aim_type == 1 ? 7 : settings.hitbox;
//	for (int i = 1; i < g_EngineClient->GetMaxClients(); ++i)
//	{
//		damage = 0.f;
//		player = (C_BasePlayer*)g_EntityList->GetClientEntity(i);
//		if (player->IsNotTarget()) {
//			continue;
//		}
//		if (!g_Options.aimbot_deathmatch && player->m_iTeamNum() == g_LocalPlayer->m_iTeamNum()) {
//			continue;
//		}
//		for (int bone = fromBone; bone <= toBone; bone++) {
//			eVecTarget = player->GetHitboxPos(bone);
//			Math::VectorAngles(eVecTarget - pVecTarget, ang);
//			Math::FixAngles(ang);
//			distance = pVecTarget.DistTo(eVecTarget);
//			if (settings.fov_type == 1)
//				fov = GetRealDistanceFOV(distance, ang, cmd);
//			else
//				fov = GetFovToPlayer(cmd->viewangles, ang);
//			if (fov > GetFov()) {
//				continue;
//			}
//			if (!g_LocalPlayer->CanSeePlayer(player, eVecTarget)) {
//				if (!settings.autowall) {
//					continue;
//				}
//				damage = Autowall::GetDamage(eVecTarget);
//				if (damage < settings.min_damage) {
//					continue;
//				}
//			}
//			if ((settings.priority == 1 || settings.priority == 2) && damage == 0.f) {
//				damage = Autowall::GetDamage(eVecTarget);
//			}
//			health = player->m_iHealth() - damage;
//			if (settings.check_smoke && IsLineGoesThroughSmoke(pVecTarget, eVecTarget)) {
//				continue;
//			}
//			if (settings.aim_type == 1 && bestBoneFov < fov) {
//				continue;
//			}
//			bestBoneFov = fov;
//			if (
//				(settings.priority == 0 && bestFov > fov) ||
//				(settings.priority == 1 && bestHealth > health) ||
//				(settings.priority == 2 && bestDamage < damage) ||
//				(settings.priority == 3 && distance < bestDistance)
//				) {
//				bestBone = bone;
//				target = player;
//				bestFov = fov;
//				bestHealth = health;
//				bestDamage = damage;
//				bestDistance = distance;
//			}
//		}
//	}
//	return target;
//}
C_BasePlayer* Aimbot::GetClosestPlayer(CUserCmd* cmd, int& bestBone) {
	QAngle ang;
	Vector eVecTarget;
	Vector pVecTarget = g_LocalPlayer->GetEyePos();

	if (target && !kill_delay && settings.kill_delay > 0 && target->IsNotTarget()) {
		target = NULL;
		shot_delay = false;
		kill_delay = true;
		kill_delay_time = (int)GetTickCount() + settings.kill_delay;
	}
	if (kill_delay) {
		if (kill_delay_time <= (int)GetTickCount()) kill_delay = false;
		else return NULL;
	}

	C_BasePlayer* player;
	target = NULL;
	aim_position = Vector{ 0, 0, 0 };
	int bestHealth = 100.f;
	float bestFov = 9999.f;
	float bestDamage = 0.f;
	float bestBoneFov = 9999.f;
	float bestDistance = 9999.f;
	int health;
	float fov;
	float damage;
	float distance;
	int fromBone = settings.aim_type == 1 ? 0 : settings.hitbox;
	int toBone = settings.aim_type == 1 ? 7 : settings.hitbox;
	QAngle backtrack_ang;
	Vector backtrack_bestpos;
	Vector backtrack_eVecTarget;
	Vector backtrack_pVecTarget = g_LocalPlayer->GetEyePos();
	float backtrack_fov = 0.f;
	float beastbacktrack_fov = 999999;

	for (int i = 1; i < g_EngineClient->GetMaxClients(); ++i) {
		damage = 0.f;
		player = (C_BasePlayer*)g_EntityList->GetClientEntity(i);
		if (player->IsNotTarget()) {
			continue;
		}
		if (!g_Options.aimbot_deathmatch && player->m_iTeamNum() == g_LocalPlayer->m_iTeamNum()) {
			continue;
		}
		for (int bone = fromBone; bone <= toBone; bone++) {
			eVecTarget = player->GetHitboxPos(bone);
			Math::VectorAngles(eVecTarget - pVecTarget, ang);
			Math::FixAngles(ang);
			distance = pVecTarget.DistTo(eVecTarget);
			if (settings.fov_type == 1)
				fov = GetRealDistanceFOV(distance, ang, cmd);
			else
				fov = GetFovToPlayer(cmd->viewangles + RCSLastPunch, ang);

			bool backtrack_enable = g_Backtrack->data.count(player->EntIndex()) > 0 && settings.enable_backtrack && settings.backtrack_ticks > 0;
			Vector local_bb_bone_pos = Vector{ 0,0,0 };

			if (backtrack_enable /*&& !settings.autowall*/ && fov > GetFov()) {
				auto& data = g_Backtrack->data.at(player->EntIndex());
				if (data.size() > 0) {
					Vector best_bb_bone_pos = Vector{ 0,0,0 };

					for (auto& record : data) {
						auto hitbox_head = record.hitboxset->GetHitbox(bone);
						auto hitbox_center = (hitbox_head->bbmin + hitbox_head->bbmax) * 0.5f;

						Math::VectorTransform(hitbox_center, record.boneMatrix[hitbox_head->bone], best_bb_bone_pos);

						backtrack_eVecTarget = best_bb_bone_pos;
						Math::VectorAngles(backtrack_eVecTarget - backtrack_pVecTarget, backtrack_ang);
						Math::FixAngles(backtrack_ang);

						backtrack_fov = GetFovToPlayer(cmd->viewangles + RCSLastPunch, backtrack_ang);

						if (backtrack_fov > (settings.silent_fov > settings.fov ? settings.silent_fov : settings.fov))
							continue;

						if (beastbacktrack_fov > backtrack_fov) {
							beastbacktrack_fov = backtrack_fov;
							local_bb_bone_pos = best_bb_bone_pos;
						}
					}

					if (g_LocalPlayer->CanSeePlayered(player, local_bb_bone_pos) > 0.9f)
						backtrack_bestpos = local_bb_bone_pos;
				}
			}

			if (fov > GetFov() && (backtrack_bestpos == Vector{ 0,0,0 } || !backtrack_enable || !backtrack_bestpos.IsValid()))
				continue;

			if (!g_LocalPlayer->CanSeePlayer(player, eVecTarget)) {

				if (!settings.autowall)
					continue;

				damage = Autowall::GetDamage(eVecTarget);
				if (damage < settings.min_damage)
					continue;

			}
			if ((settings.priority == 1 || settings.priority == 2) && damage == 0.f)
				damage = Autowall::GetDamage(eVecTarget);

			health = player->m_iHealth() - damage;
			if (settings.check_smoke && IsLineGoesThroughSmoke(pVecTarget, eVecTarget))
				continue;

			if (settings.aim_type == 1 && bestBoneFov < fov) {
				continue;
			}
			bestBoneFov = fov;

			if (backtrack_bestpos != Vector{ 0,0,0 } && backtrack_bestpos.IsValid()) {
				aim_position = backtrack_bestpos;
			}

			if (
				(settings.priority == 0 && bestFov > fov) ||
				(settings.priority == 1 && bestHealth > health) ||
				(settings.priority == 2 && bestDamage < damage) ||
				(settings.priority == 3 && distance < bestDistance)
				) {
				bestBone = bone;
				target = player;
				bestFov = fov;
				bestHealth = health;
				bestDamage = damage;
				bestDistance = distance;
			}


		}
	}
	return target;
}

//--------------------------------------------------------------------------------
bool Aimbot::IsNotSilent(float fov)
{
	return IsRcs() || !silent_enabled || (silent_enabled && fov > settings.silent_fov);
}
//--------------------------------------------------------------------------------
//void Aimbot::OnMove(CUserCmd* pCmd, bool* bSendPacket) {
//	if (!g_LocalPlayer || !g_EngineClient->IsInGame() || !g_LocalPlayer->IsAlive() || !g_EngineClient->IsConnected()) return;
//
//	auto weapon = g_LocalPlayer->m_hActiveWeapon().Get();
//	if (!weapon)
//		return;
//
//	auto weapon_data = weapon->GetCSWeaponData();
//	if (!weapon_data)
//		return;
//	
//	short index = weapon->m_Item().m_iItemDefinitionIndex();
//	settings = *g_CustomWeaponGroups->GetSettings(index);
//
//	if (!IsEnabled(pCmd)) {
//		if (settings.rcs_type == 0) {
//			auto pWeapon = g_LocalPlayer->m_hActiveWeapon();
//			if (pWeapon && pWeapon->GetCSWeaponData() && (pWeapon->IsSniper() || pWeapon->IsPistol() || pWeapon->IsRifle())) {
//				RCS(pCmd->viewangles, target, false);
//				Math::FixAngles(pCmd->viewangles);
//				g_EngineClient->SetViewAngles(&pCmd->viewangles);
//			}
//		}
//		else {
//			RCSLastPunch = { 0, 0, 0 };
//		}
//
//		is_delayed = false;
//		shot_delay = false;
//		kill_delay = false;
//		silent_enabled = (settings.silent || settings.psilent) && settings.silent_fov > 0;
//		target = NULL;
//		return;
//	}
//	QAngle oldAngle;
//	g_EngineClient->GetViewAngles(&oldAngle);
//	RandomSeed(pCmd->command_number);
//
//	bool should_do_rcs = false;
//	QAngle angles = pCmd->viewangles;
//	QAngle current = angles;
//	float fov = 180.f;
//	int bestBone = -1;
//	if (!(settings.flash_check && g_LocalPlayer->IsFlashed())) {
//		C_BasePlayer* player = GetClosestPlayer(pCmd, bestBone);
//		if (player) {
//			Vector eVecTarget;
//			eVecTarget = target->GetHitboxPos(bestBone);
//			if (aim_position != Vector{ 0,0,0 })
//				eVecTarget = aim_position;
//			angles = Math::CalcAngle(g_LocalPlayer->GetEyePos(), eVecTarget);
//
//			if (settings.smooth > 2 && settings.humanize) {
//				float dist = Math::NormalizeYaw(angles.yaw - oldAngle.yaw);
//				if (dist > 180.0f) dist = 360.0f - dist;
//				eVecTarget += Vector(0, 0, settings.curve * dist);
//
//				angles = Math::CalcAngle(g_LocalPlayer->GetEyePos(), eVecTarget);
//			}
//
//			Math::FixAngles(angles);
//
//			if (settings.fov_type == 1)
//				fov = GetRealDistanceFOV(g_LocalPlayer->GetEyePos().DistTo(eVecTarget), angles, pCmd->viewangles);
//			else
//				fov = GetFovToPlayer(pCmd->viewangles, angles);
//
//			should_do_rcs = true;
//
//			if (!settings.silent && !is_delayed && !shot_delay && settings.shot_delay > 0 && !settings.psilent) {
//				is_delayed = true;
//				shot_delay = true;
//				shot_delay_time = GetTickCount() + settings.shot_delay;
//			}
//
//			if (shot_delay && shot_delay_time <= GetTickCount()) {
//				shot_delay = false;
//			}
//
//			if (shot_delay) {
//				pCmd->buttons &= ~IN_ATTACK;
//			}
//
//			if (settings.autostop) {
//				Vector Velocity = g_LocalPlayer->m_vecVelocity();
//
//				if (Velocity.Length2D() != 0) {
//
//					static float Speed = 450.f;
//
//					QAngle Direction;
//					QAngle RealView;
//					Math::VectorAngles(Velocity, Direction);
//					g_EngineClient->GetViewAngles(&RealView);
//					Direction.yaw = RealView.yaw - Direction.yaw;
//
//					Vector Forward;
//					Math::AngleVectors(Direction, Forward);
//					Vector NegativeDirection = Forward * -Speed;
//
//					pCmd->forwardmove = NegativeDirection.x;
//					pCmd->sidemove = NegativeDirection.y;
//				}
//			}
//		}
//	}
//
//	if (IsNotSilent(fov) && (should_do_rcs || settings.rcs_type == 0)) {
//		RCS(angles, target, should_do_rcs);
//	}
//	if (target && IsNotSilent(fov)) {
//		Smooth(current, angles, angles);
//	}
//
//
//	Math::FixAngles(angles);
//	pCmd->viewangles = angles;
//
//	if (IsNotSilent(fov)) {
//		g_EngineClient->SetViewAngles(&angles);
//	}
//}
//
//	silent_enabled = false;
//	if (settings.autopistol) {
//		const auto activeWeapon = g_LocalPlayer->m_hActiveWeapon();
//		if (activeWeapon && pCmd->buttons & IN_ATTACK && activeWeapon->CanFire()) {
//			static bool fire = false;
//			if (fire)
//				pCmd->buttons &= ~IN_ATTACK;
//			else
//				pCmd->buttons |= IN_ATTACK;
//			fire = !fire;
//		}
//	}
//}

void Aimbot::OnMove(CUserCmd* pCmd)
{
	if (!g_LocalPlayer || !g_EngineClient->IsInGame() || !g_LocalPlayer->IsAlive() || !g_EngineClient->IsConnected()) return;
	
	auto weapon = g_LocalPlayer->m_hActiveWeapon().Get();
	if (!weapon)
		return;

	short index = weapon->m_Item().m_iItemDefinitionIndex();
	settings = *g_CustomWeaponGroups->GetSettings(index);

	if (!IsEnabled(pCmd)) {
		RCSLastPunch = { 0, 0, 0 };
		is_delayed = false;
		shot_delay = false;
		kill_delay = false;
		silent_enabled = settings.silent && settings.silent_fov > 0;
		target = NULL;
		return;
	}
	QAngle angles = pCmd->viewangles;
	QAngle current = angles;
	float fov = 180.f;
	if (!(settings.check_flash && g_LocalPlayer->IsFlashed())) {
		int bestBone = -1;
		if (GetClosestPlayer(pCmd, bestBone)) {
			Math::VectorAngles(target->GetHitboxPos(bestBone) - g_LocalPlayer->GetEyePos(), angles);
			Math::FixAngles(angles);
			if (settings.fov_type == 1)
				fov = GetRealDistanceFOV(g_LocalPlayer->GetEyePos().DistTo(target->GetHitboxPos(bestBone)), angles, pCmd);
			else
				fov = GetFovToPlayer(pCmd->viewangles, angles);
			if (!settings.silent && !is_delayed && !shot_delay && settings.shot_delay > 0) {
				is_delayed = true;
				shot_delay = true;
				shot_delay_time = GetTickCount() + settings.shot_delay;
			}
			if (shot_delay && shot_delay_time <= GetTickCount()) {
				shot_delay = false;
			}
			if (shot_delay) {
				pCmd->buttons &= ~IN_ATTACK;
			}
		}
	}
	CurrentPunch = g_LocalPlayer->m_aimPunchAngle();
	if (IsNotSilent(fov)) {
		RCS(angles, target);
	}
	RCSLastPunch = CurrentPunch;
	if (target && IsNotSilent(fov)) {
		Smooth(current, angles, angles);
	}
	Math::FixAngles(angles);
	pCmd->viewangles = angles;
	if (IsNotSilent(fov)) {
		g_EngineClient->SetViewAngles(&angles);
	}

	silent_enabled = false;
	if (g_LocalPlayer->m_hActiveWeapon()->IsPistol() && settings.autopistol) {
		float server_time = g_LocalPlayer->m_nTickBase() * g_GlobalVars->interval_per_tick;
		float next_shot = g_LocalPlayer->m_hActiveWeapon()->m_flNextPrimaryAttack() - server_time;
		if (next_shot > 0) {
			pCmd->buttons &= ~IN_ATTACK;
		}
	}
}

void Aimbot::SetupValues() {
	g_Config->PushItem(&g_Options.aimbot_enable, "aim", "enable", g_Options.aimbot_enable);
	g_Config->PushItem(&g_Options.aimbot_deathmatch, "aim", "deathmatch", g_Options.aimbot_deathmatch);
	g_Config->PushItem(&g_Options.aimbot_on_key, "aim", "on_key", g_Options.aimbot_on_key);
	g_Config->PushItem(&g_Options.aimbot_key, "aim", "key", g_Options.aimbot_key);
}

Aimbot g_Aimbot;
