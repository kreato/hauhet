#include "chams.hpp"
#include "../hooks/hooks.hpp"
#include "../sdk/render/ui.hpp"

Chams* g_Chams;


Chams::Chams() noexcept
{
    KeyValues* normalKV = new KeyValues("chamsNormal");
    normalKV->LoadFromBuffer(normalKV, "chamsNormal", "VertexLitGeneric { }");
    normal = g_MatSystem->CreateMaterial("chamsNormal", normalKV);
    normal->IncrementReferenceCount();
    
    KeyValues* flatKV = new KeyValues("chamsFlat");
    flatKV->LoadFromBuffer(flatKV, "chamsFlat", "UnlitGeneric { }");
    flat = g_MatSystem->CreateMaterial("chamsFlat", flatKV);
    flat->IncrementReferenceCount();

    KeyValues* animatedKV = new KeyValues("chamsAnimated");
    animatedKV->LoadFromBuffer(animatedKV, "chamsAnimated", "VertexLitGeneric { $envmap editor/cube_vertigo $envmapcontrast 1 $envmaptint \"[.7 .7 .7]\" $basetexture dev/zone_warning proxies { texturescroll { texturescrollvar $basetexturetransform texturescrollrate 0.6 texturescrollangle 90 } } }");
    animated = g_MatSystem->CreateMaterial("chamsAnimated", animatedKV);
    animated->IncrementReferenceCount();
    
    glass = g_MatSystem->FindMaterial("models/inventory_items/cologne_prediction/cologne_prediction_glass", TEXTURE_GROUP_MODEL);
    
    crystal = g_MatSystem->FindMaterial("models/inventory_items/trophy_majors/crystal_clear", TEXTURE_GROUP_MODEL);
    
    silver = g_MatSystem->FindMaterial("models/inventory_items/trophy_majors/silver_plain", TEXTURE_GROUP_MODEL);
    
    glow_armsrace = g_MatSystem->FindMaterial("dev/glow_armsrace", TEXTURE_GROUP_MODEL);

    KeyValues* glowKV = new KeyValues("chamsGlow");
    glowKV->LoadFromBuffer(glowKV, "chamsGlow", R"#("VertexLitGeneric" 
        {
          "$additive" "1"
          "$envmap" "models/effects/cube_white"
          "$envmaptint" "[1 1 1]"
          "$envmapfresnel" "1"
          "$envmapfresnelminmaxexp" "[0 1 2]"
          "$alpha" "1"  
        })#");
    glow = g_MatSystem->CreateMaterial("chamsGlow", glowKV);
    glow->IncrementReferenceCount();

    gloss = g_MatSystem->FindMaterial("models/inventory_items/trophy_majors/gloss", TEXTURE_GROUP_MODEL);
}

bool Chams::render(IMatRenderContext* ctx, const DrawModelState_t& state, const ModelRenderInfo_t& info, matrix3x4_t* customBoneToWorld) const noexcept
{
    if (std::strstr(info.pModel->szName, "models/player"))
        return renderPlayers(ctx, state, info, customBoneToWorld);
    else if (std::strstr(info.pModel->szName, "sleeve"))
        renderSleeves(ctx, state, info, customBoneToWorld);
    else if (std::strstr(info.pModel->szName, "arms"))
        renderHands(ctx, state, info, customBoneToWorld);
    else if (std::strstr(info.pModel->szName, "models/weapons/v_")
        && !std::strstr(info.pModel->szName, "tablet")
        && !std::strstr(info.pModel->szName, "parachute")
        && !std::strstr(info.pModel->szName, "fists"))
        renderWeapons(ctx, state, info, customBoneToWorld);
    return true;
}

bool Chams::renderPlayers(IMatRenderContext* ctx, const DrawModelState_t& state, const ModelRenderInfo_t& info, matrix3x4_t* customBoneToWorld) const noexcept
{
    if (g_MdlRender->IsForcedMaterialOverride())
        return true;

    C_BasePlayer* entity = (C_BasePlayer*)g_EntityList->GetClientEntity(info.entity_index);
    if (!entity || entity->IsDormant() || !entity->IsPlayer()) return true;

    bool needRedraw = true;
    static auto fnDME = hooks::mdlrender_hook.get_original<decltype(&hooks::hkDrawModelExecute)>(index::DrawModelExecute);

    auto applied{ false };
    for (size_t i = 0; i < chams_player_allies_all.materials.size(); ++i) {
        if (chams_player_backtrack.materials[i].enabled && !g_Backtrack->data.empty() && entity->m_iTeamNum() != g_LocalPlayer->m_iTeamNum()) {
            if (applied || chams_player_backtrack.DoNotRemoveModel)
                fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                 auto data = g_Backtrack->data[entity->EntIndex()];
                 if (data.size() > 0) {
                     if (g_Options.chams_backtrack_all_ticks) {
                         for (auto& record : data) {
                             applyChams(chams_player_backtrack.materials[i], true, entity->m_iHealth());
                             fnDME(g_MdlRender, 0, ctx, state, std::cref(info), record.boneMatrix);
                             g_MdlRender->ForcedMaterialOverride(nullptr);
                             applied = true;
                         }
                     }
                     else {
                         auto& back = data.back();
                         applyChams(chams_player_backtrack.materials[i], true, entity->m_iHealth());
                         fnDME(g_MdlRender, 0, ctx, state, std::cref(info), back.boneMatrix);
                         g_MdlRender->ForcedMaterialOverride(nullptr);
                         applied = true;
                     }
                 }
        }
        if (entity == g_LocalPlayer) {
            if (chams_player_local_player.materials[i].enabled) {
                if (applied || chams_player_local_player.DoNotRemoveModel)
                    fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                applyChams(chams_player_local_player.materials[i], false, entity->m_iHealth());
                applied = true;
            }
        }
        else if (entity->m_iTeamNum() != g_LocalPlayer->m_iTeamNum()) {
            if (chams_player_enemies_all.materials[i].enabled) {
                if (applied || chams_player_enemies_all.DoNotRemoveModel)
                    fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                applyChams(chams_player_enemies_all.materials[i], true, entity->m_iHealth());
                fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                applyChams(chams_player_enemies_all.materials[i], false, entity->m_iHealth());
                fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                needRedraw = false;
                applied = true;
            }
            else {
                if (chams_player_enemies_occluded.materials[i].enabled) {
                    if (applied || chams_player_enemies_occluded.DoNotRemoveModel)
                        fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                    applyChams(chams_player_enemies_occluded.materials[i], true, entity->m_iHealth());
                    fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                    if (!chams_player_enemies_visible.materials[i].enabled)
                        g_MdlRender->ForcedMaterialOverride(nullptr);
                    applied = true;
                }
                if (chams_player_enemies_visible.materials[i].enabled) {
                    if (!g_Aimbot.IsLineGoesThroughSmoke(g_LocalPlayer->GetEyePos(), entity->GetHitboxPos(HITBOX_CHEST))) {
                        if (applied || chams_player_enemies_visible.DoNotRemoveModel)
                            fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                        applyChams(chams_player_enemies_visible.materials[i], false, entity->m_iHealth());
                        fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                    }
                    needRedraw = false;
                    applied = true;
                }
            }
        }
        else {
            if (chams_player_allies_all.materials[i].enabled) {
                if (applied || chams_player_allies_all.DoNotRemoveModel)
                    fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                applyChams(chams_player_allies_all.materials[i], true, entity->m_iHealth());
                fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                applyChams(chams_player_allies_all.materials[i], false, entity->m_iHealth());
                applied = true;
            }
            else {
                if (chams_player_allies_occluded.materials[i].enabled) {
                    if (applied || chams_player_allies_occluded.DoNotRemoveModel)
                        fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                    applyChams(chams_player_allies_occluded.materials[i], true, entity->m_iHealth());
                    fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                    if (!chams_player_allies_visible.materials[i].enabled)
                        g_MdlRender->ForcedMaterialOverride(nullptr);
                    applied = true;
                }
                if (chams_player_allies_visible.materials[i].enabled) {
                    if (applied || chams_player_allies_visible.DoNotRemoveModel)
                        fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
                    applyChams(chams_player_allies_visible.materials[i], false, entity->m_iHealth());
                    applied = true;
                }
            }
        }
    }
    return needRedraw;
}

void Chams::renderWeapons(IMatRenderContext* ctx, const DrawModelState_t& state, const ModelRenderInfo_t& info, matrix3x4_t* customBoneToWorld) const noexcept
{
    if (!g_LocalPlayer || !g_LocalPlayer->IsAlive() || g_LocalPlayer->m_bIsScoped())
        return;

    static auto fnDME = hooks::mdlrender_hook.get_original<decltype(&hooks::hkDrawModelExecute)>(index::DrawModelExecute);

    bool applied = false;
    for (size_t i = 0; i < chams_weapons.materials.size(); ++i) {
        if (chams_weapons.materials[i].enabled) {
            if (applied || chams_weapons.DoNotRemoveModel)
                fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
            applyChams(chams_weapons.materials[i], false, g_LocalPlayer->m_iHealth());
            applied = true;
        }
    }
}

void Chams::renderHands(IMatRenderContext* ctx, const DrawModelState_t& state, const ModelRenderInfo_t& info, matrix3x4_t* customBoneToWorld) const noexcept
{
    if (!g_LocalPlayer || !g_LocalPlayer->IsAlive()) return;

    static auto fnDME = hooks::mdlrender_hook.get_original<decltype(&hooks::hkDrawModelExecute)>(index::DrawModelExecute);

    bool applied = false;
    for (size_t i = 0; i < chams_hands.materials.size(); ++i) {
        if (chams_hands.materials[i].enabled) {
            if (applied || chams_hands.DoNotRemoveModel)
                fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
            applyChams(chams_hands.materials[i], false, g_LocalPlayer->m_iHealth());
            applied = true;
        }
    }
}

void Chams::renderSleeves(IMatRenderContext* ctx, const DrawModelState_t& state, const ModelRenderInfo_t& info, matrix3x4_t* customBoneToWorld) const noexcept
{
    if (!g_LocalPlayer || !g_LocalPlayer->IsAlive())
        return;

    static auto fnDME = hooks::mdlrender_hook.get_original<decltype(&hooks::hkDrawModelExecute)>(index::DrawModelExecute);

    bool applied = false;
    for (size_t i = 0; i < chams_sleeves.materials.size(); ++i) {
        if (chams_sleeves.materials[i].enabled) {
            if (applied || chams_sleeves.DoNotRemoveModel)
                fnDME(g_MdlRender, 0, ctx, state, std::cref(info), customBoneToWorld);
            applyChams(chams_sleeves.materials[i], false, g_LocalPlayer->m_iHealth());
            applied = true;
        }
    }
}