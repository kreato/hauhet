#pragma once
#include "../options.hpp"
#include "../sdk/csgostructs.hpp"
#include "backtrack.hpp"
#include "../sdk/render/ui.hpp"
class Aimbot {
public:
	void OnMove(CUserCmd* pCmd);
	bool IsLineGoesThroughSmoke(Vector vStartPos, Vector vEndPos);
private:
	legitbot_settings settings;
	Vector aim_position = { 0, 0, 0 };

	bool IsRcs();
	float GetSmooth();
	float GetFov();
	void RCS(QAngle& angle, C_BasePlayer* target);
	bool IsEnabled(CUserCmd* pCmd);
	float GetFovToPlayer(QAngle viewAngle, QAngle aimAngle);
	void Smooth(QAngle currentAngle, QAngle aimAngle, QAngle& angle);
	bool IsNotSilent(float fov);
	C_BasePlayer* GetClosestPlayer(CUserCmd* cmd, int& bestBone);
	float shot_delay_time;
	bool shot_delay = false;
	bool silent_enabled = false;
	QAngle CurrentPunch = { 0,0,0 };
	QAngle RCSLastPunch = { 0,0,0 };
	bool is_delayed = false;
	int kill_delay_time;
	bool kill_delay = false;
	C_BasePlayer* target = NULL;
public:
	void SetupValues();
};
//extern Aimbot g_Aimbot;
//class Aimbot {
//public:	
//	bool IsLineGoesThroughSmoke(Vector vStartPos, Vector vEndPos);
//private:
//	bool IsEnabled(CUserCmd* pCmd);
//	float GetFovToPlayer(QAngle viewAngle, QAngle aimAngle);
//	bool IsRcs();
//	float GetSmooth();
//	float GetFov();
//	void RCS(QAngle& angle, C_BasePlayer* target, bool should_run);
//	void Smooth(QAngle currentAngle, QAngle aimAngle, QAngle& angle);
//	bool IsNotSilent(float fov);
//	C_BasePlayer* GetClosestPlayer(CUserCmd* cmd, int& bestBone);
//	legitbot_settings settings;
//	float shot_delay_time;
//	bool shot_delay = false;
//	bool silent_enabled = false;
//	QAngle CurrentPunch = { 0,0,0 };
//	QAngle RCSLastPunch = { 0,0,0 };
//	bool is_delayed = false;
//	int kill_delay_time;
//	bool kill_delay = false;
//	C_BasePlayer* target = NULL;
//	Vector aim_position = { 0, 0, 0 };
//public:
//	void MenuAimbot();
//	void SetupValues();
//};

extern Aimbot g_Aimbot;
