#pragma once
#include <string>
#include <memory>
#include <unordered_map>
#include <map>
#include <vector>
#include <functional>
#include "sdk/Misc/Color.hpp"
#include <set>
#include "sdk/misc/Enums.hpp"
#include "features/chams.hpp"
#include "features/keybinds.hpp"
#include "sdk/Misc/Color.hpp"
#include <algorithm>
#include "menu/imgui/imgui.h"
#define A( s ) #s
#define OPTION(type, var, val) Var<type> var = {A(var), val}

template <typename T = bool>
class Var {
public:
	std::string name;
	std::shared_ptr<T> value;
	int32_t size;
	Var(std::string name, T v) : name(name) {
		value = std::make_shared<T>(v);
		size = sizeof(T);
	}
	operator T() { return *value; }
	operator T* () { return &*value; }
	operator T() const { return *value; }
	//operator T*() const { return value; }
};
namespace values {
	extern const char* aim_types[];
	extern const char* smooth_types[];
	extern const char* hitbox_list[];
	extern const char* rcs_types[];
	extern const char* priorities[];
	extern const char* fov_types[];

	extern std::map<short, std::string> WNames;
};

struct weapon_name_t {
	constexpr weapon_name_t(int32_t definition_index, const char* name) :
		definition_index(definition_index),
		name(name) {
	}

	int32_t definition_index = 0;
	const char* name = nullptr;
};
struct weapon_info_t {
	constexpr weapon_info_t(const char* model, const char* icon = nullptr) :
		model(model),
		icon(icon)
	{}

	const char* model;
	const char* icon;
};

namespace g_Change {
	extern const std::map<size_t, weapon_info_t> WeaponInfo;
	extern const std::vector<weapon_name_t> KnifeNames;
	extern const std::vector<weapon_name_t> GloveNames;
	extern std::vector<weapon_name_t> WeaponNamesFull;

	extern const char* GetWeaponNameById(int id);
	extern int GetKnifeDefinitionIndex(int si);
	extern int GetGloveDefinitionIndex(int si);
};

struct item_setting {
	char name[32] = "";
	int definition_vector_index = 0;
	int definition_index = 1;
	int paint_kit_vector_index = 0;
	int paint_kit_index = 0;
	int definition_override_vector_index = 0;
	int definition_override_index = 0;
	int seed = 0;
	bool enable_stat_track = false;
	int stat_trak = 0;
	float wear = 0.0f;
};
struct skinInfo {
	int seed = -1;
	int paintkit;
	int rarity;
	std::string tagName;
	std::string     shortname; // shortname
	std::string     name;      // full skin name
};
struct legitbot_settings {
	bool autopistol = false;
	bool check_smoke = false;
	bool check_flash = false;
	bool autowall = false;
	bool silent = false;
	bool rcs = false;
	bool rcs_fov_enabled = false;
	bool rcs_smooth_enabled = false;
	bool humanize = false;
	bool enable_backtrack = false;
	int  backtrack_ticks = 0;
	bool only_in_zoom = true;
	int aim_type = 1;
	int priority = 0;
	int fov_type = 0;
	int rcs_type = 0;
	int hitbox = 1;
	float fov = 0.f;
	float silent_fov = 0.f;
	float rcs_fov = 0.f;
	float smooth = 1;
	float rcs_smooth = 1;
	int shot_delay = 0;
	int kill_delay = 0;
	int rcs_x = 100;
	int rcs_y = 100;
	int rcs_start = 1;
	int min_damage = 1;
};

class Options
{
public:
	bool aimbot_enable = false;
	bool aimbot_deathmatch = false;

	bool aimbot_on_key = false;
	bool aimbot_key_pressed = false;
	CKeyBind aimbot_key = CKeyBind(&aimbot_key_pressed, &aimbot_on_key, "Aimbot");

	bool auto_fire = false;
	bool auto_fire_key_pressed = false;
	CKeyBind auto_fire_key = CKeyBind(&auto_fire_key_pressed, &auto_fire, "Auto Fire");
	// 
	// BOX
	// 
	bool enabled_box = false;
	bool box_outline = false;
	bool box_filled = false;

	int box_type = 0;

	Color box_outline_clr = Color(0.f, 0.f, 0.f, 1.f);

	Color box_allies_vis_clr = Color(255, 255, 255);
	Color box_enemy_vis_clr = Color(255, 255, 255);
	Color box_allies_invis_clr = Color(255, 255, 255);
	Color box_enemy_invis_clr = Color(255, 255, 255);

	Color box_fill_color = Color(255, 255, 255);


	// 
	// SKELETON
	// 
	bool enabled_skeleton = false;
	Color skeleton_clr = Color(255, 255, 255);

	// 
	// ESP_OTHER
	// 
	bool hp = false;
	bool name = false;
	bool weapon = false;
	bool ammo_bar = false;
	bool armor = false;
	bool snipeline = false;
	bool head_dot = false;
	float nightmode_value = 1.1f;
	bool nosmoke = false;
	bool no_visual_recoil = false;

	bool team = false;
	bool visible_only = false;
	bool dormant = false;
	bool show_keybinds = false;
	bool molotov_timer = false;

	bool  flags = false;
	bool  flags_scoped = false;
	float flags_scoped_size = 8.f;
	bool  flags_reload = false;
	float flags_reload_size = 8.f;
	bool  flags_bomb = false;
	float flags_bomb_size = 8.f;
	bool  flags_flashed = false;
	float flags_flashed_size = 8.f;
	bool  flags_hk = false;
	float flags_hk_size = 8.f;
	bool  flags_defusing = false;
	float flags_defusing_size = 8.f;

	bool  sound_esp = false;
	int   sound_esp_type = 0;
	bool  sound_esp_onteam = false;
	float sound_esp_time = 0.5f;
	float sound_esp_radius = 15.f;

	float name_size = 8.f;
	float weapon_size = 8.f;

	Color name_clr = Color(255, 255, 255);
	Color weapon_clr = Color(255, 255, 255);
	Color snipeline_clr = Color(255, 255, 255);
	Color head_dot_clr = Color(255, 255, 255);
	Color sound_esp_clr = Color(255, 255, 255);
	Color hp_clr = Color(0, 255, 0);
	Color armor_clr = Color(0, 50, 255);
	Color ammobar_clr = Color(255, 255, 255);


	// 
	// GLOW
	// 
	bool enabled_glow = false;

	bool players = false;
	bool on_team = false;
	bool on_chickens = false;
	bool c4_carrier = false;
	bool c4_planted = false;
	bool defuse_kits = false;
	bool weapons = false;

	int type_glow = 0;
	bool bloom_glow = false;

	Color enemy_clr = Color(255, 255, 255);
	Color enemy_invis_clr = Color(255, 255, 255);

	Color allies_clr = Color(255, 255, 255);
	Color allies_invis_clr = Color(255, 255, 255);

	Color chickens_clr = Color(255, 255, 255);
	Color c4_carrier_clr = Color(255, 255, 255);
	Color c4_planted_clr = Color(255, 255, 255);
	Color defuse_kits_clr = Color(255, 255, 255);
	Color weapons_clr = Color(255, 255, 255);

	//
	// CHAMS
	//
	int menuMaterial = 1;
	bool chams_backtrack_all_ticks = true;
	// 
	// CHANGER
	// 
	static auto is_knife(const int i) -> bool {
		return (i >= WEAPON_KNIFE_BAYONET && i < GLOVE_STUDDED_BLOODHOUND) || i == WEAPON_KNIFE_T || i == WEAPON_KNIFE;
	}
	int weapon_index_skins = 7;
	int weapon_vector_index_skins = 0;
	std::unordered_map<std::string, std::set<std::string>> weaponSkins;
	std::unordered_map<std::string, skinInfo> skinMap;
	std::unordered_map<std::string, std::string> skinNames;
	std::map<int, item_setting> Items;
	std::unordered_map<std::string, std::string> IconOverrides;

	//
	// MISC
	//
	OPTION(bool, eventlogger_hurt, false);
	OPTION(bool, eventlogger_player_buy, false);
	OPTION(bool, eventlogger_planting, false);
	OPTION(bool, eventlogger_defusing, false);
	OPTION(bool, misc_desync, false);
	OPTION(int, misc_desync_hotkey, 0);
	bool  misc_spectators = false;
	float flVelocityGraphCompression = 0.f;
	float bloom_value = 0.f;
	float model_ambient_value = 0,f;
	OPTION(bool, misc_bhop, false);
	OPTION(bool, misc_jumpbug, false);
	OPTION(bool, edge_bug, false);
	OPTION(bool, edgejump, false);
	OPTION(bool, edgejump_duck_in_air, false);
	OPTION(int, edgejump_hotkey, 0);
	OPTION(int, edgebug_hotkey, 0);
	OPTION(int, misc_jumpbug_hotkey, 0);
	OPTION(bool, misc_fullbright, false);
	OPTION(float, misc_nightmode, 1.f);
	OPTION(float, misc_aspect_ratio, 0);
	OPTION(bool, misc_thirdperson, false);
	OPTION(bool, misc_remove_scope, false);
	OPTION(int, misc_thirdperson_hotkey, 0);
	OPTION(bool, misc_no_hands, false);
	OPTION(bool, hauhet, false);
	OPTION(bool, misc_watermark, true);
	OPTION(bool, misc_rpc, false);
	OPTION(int, viewmodel_fov, 68);
	OPTION(int, viewmodel_offset_x, 1);
	OPTION(int, viewmodel_offset_y, 1);
	OPTION(int, viewmodel_offset_z, -1);
	OPTION(float, mat_ambient_light_r, 0.0f);
	OPTION(float, mat_ambient_light_g, 0.0f);
	OPTION(float, mat_ambient_light_b, 0.0f);

	// 
	// COLORS
	// 
	Color accent_color = Color(255, 255, 255, 255);
	Color molotov_timer_color = Color(255, 255, 255, 255);


	OPTION(Color, color_chams_player_ally_visible, Color(0, 128, 255));
	OPTION(Color, color_chams_player_ally_occluded, Color(0, 255, 128));
	OPTION(Color, color_chams_player_enemy_visible, Color(255, 0, 0));
	OPTION(Color, color_chams_player_enemy_occluded, Color(255, 128, 0));
	OPTION(Color, color_chams_arms_visible, Color(0, 128, 255));
	OPTION(Color, color_chams_arms_occluded, Color(0, 128, 255));
	OPTION(Color, color_watermark, Color(0, 128, 255)); // no menu config cuz its useless

};

extern bool g_ShowPrompt;
extern std::string g_PromptTitle;
extern std::function<void()> g_PromptCallback;

inline Options g_Options;
extern bool   g_Unload;