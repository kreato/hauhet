#include "../hooks.hpp"

void __fastcall hooks::hkFrameStageNotify(void* _this, int edx, ClientFrameStage_t stage)
{
	static auto ofunc = hlclient_hook.get_original<decltype(&hkFrameStageNotify)>(index::FrameStageNotify);
	if (g_EngineClient->IsInGame()) {

		if (stage == FRAME_NET_UPDATE_POSTDATAUPDATE_START)
			g_SkinChanger->OnFrameStageNotify(false);
		else if (stage == FRAME_NET_UPDATE_POSTDATAUPDATE_END)
			g_SkinChanger->OnFrameStageNotify(true);
		else if (stage == FRAME_NET_UPDATE_START)
			g_Visuals->nosmoke();
	}
	ofunc(g_CHLClient, edx, stage);
}