#include "../hooks.hpp"

void __fastcall hooks::hkPaintTraverse(void* _this, int edx, vgui::VPANEL panel, bool forceRepaint, bool allowForce)
{
	static auto panelId = vgui::VPANEL{ 0 };
	static auto oPaintTraverse = vguipanel_hook.get_original<decltype(&hkPaintTraverse)>(index::PaintTraverse);

	oPaintTraverse(g_VGuiPanel, edx, panel, forceRepaint, allowForce);

	if (!panelId) {
		const auto panelName = g_VGuiPanel->GetName(panel);
		if (!strcmp(panelName, "FocusOverlayPanel")) {
			panelId = panel;
		}
	}
	else if (panelId == panel)
	{
		//Ignore 50% cuz it called very often
		static bool bSkip = false;
		bSkip = !bSkip;

		if (bSkip)
			return;

		g_Visuals->Bloom();
		Render::Get().BeginScene();
	}
}
