#include "../hooks.hpp"

void __fastcall hooks::hkLockCursor(void* _this)
{
	static auto ofunc = vguisurf_hook.get_original<decltype(&hkLockCursor)>(index::LockCursor);

	if (Menu::Get().IsVisible()) {
		g_VGuiSurface->UnlockCursor();
		g_InputSystem->ResetInputState();
		return;
	}
	ofunc(g_VGuiSurface);

}