#include "../hooks.hpp"

void hooks::hkRecvProxy(const CRecvProxyData* pData, void* entity, void* output)
{
	static auto oRecv = sequence_hook->GetOriginalFunction();

	if (g_LocalPlayer && g_LocalPlayer->IsAlive()) {
		const auto proxy_data = const_cast<CRecvProxyData*>(pData);
		const auto view_model = static_cast<C_BaseViewModel*>(entity);

		if (view_model && view_model->m_hOwner() && view_model->m_hOwner().IsValid()) {
			const auto owner = static_cast<C_BasePlayer*>(g_EntityList->GetClientEntityFromHandle(view_model->m_hOwner()));
			if (owner == g_EntityList->GetClientEntity(g_EngineClient->GetLocalPlayer())) {
				const auto view_model_weapon_handle = view_model->m_hWeapon();
				if (view_model_weapon_handle.IsValid()) {
					const auto view_model_weapon = static_cast<C_BaseAttributableItem*>(g_EntityList->GetClientEntityFromHandle(view_model_weapon_handle));
					if (view_model_weapon) {
						if (g_Change::WeaponInfo.count(view_model_weapon->m_Item().m_iItemDefinitionIndex())) {
							auto original_sequence = proxy_data->m_Value.m_Int;
							const auto override_model = g_Change::WeaponInfo.at(view_model_weapon->m_Item().m_iItemDefinitionIndex()).model;
							proxy_data->m_Value.m_Int = g_KnifeAnimFix->Fix(override_model, proxy_data->m_Value.m_Int);
						}
					}
				}
			}
		}

	}

	oRecv(pData, entity, output);
}