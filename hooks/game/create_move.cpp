#include "../hooks.hpp"

bool __stdcall hooks::hkCreateMove(float InputSampleFrameTime, CUserCmd* cmd)
{
	bool* bSendPacket = reinterpret_cast<bool*>(reinterpret_cast<uintptr_t>(_AddressOfReturnAddress()) + 0x14);
	auto oCreateMove = clientmode_hook.get_original< CreateMoveClientMode >(index::ClientModeCreateMove);
	bool result = oCreateMove(g_ClientMode, InputSampleFrameTime, cmd);

	if (!cmd || !cmd->command_number || g_Unload)
		return result;

	if (Menu::Get().IsVisible())
		cmd->buttons &= ~IN_ATTACK;

	if (g_Options.misc_bhop)
		BunnyHop::OnCreateMove(cmd);

	CPredictionSystem::Get().StartPrediction(g_LocalPlayer, cmd); {
		g_Aimbot.OnMove(cmd);
		g_Backtrack->CMove(cmd);
	//	g_Misc->create_move_prediction(cmd, bSendPacket);

		static ConVar* m_yaw = m_yaw = g_CVar->FindVar("m_yaw");
		static ConVar* m_pitch = m_pitch = g_CVar->FindVar("m_pitch");
		static ConVar* sensitivity = sensitivity = g_CVar->FindVar("sensitivity");

		static QAngle m_angOldViewangles = g_ClientState->viewangles;

		float delta_x = std::remainderf(cmd->viewangles.pitch - m_angOldViewangles.pitch, 360.0f);
		float delta_y = std::remainderf(cmd->viewangles.yaw - m_angOldViewangles.yaw, 360.0f);

		if (delta_x != 0.0f) {
			float mouse_y = -((delta_x / m_pitch->GetFloat()) / sensitivity->GetFloat());
			short mousedy;
			if (mouse_y <= 32767.0f)
				if (mouse_y >= -32768.0f)
					if (mouse_y >= 1.0f || mouse_y < 0.0f) {
						if (mouse_y <= -1.0f || mouse_y > 0.0f)
							mousedy = static_cast<short>(mouse_y);
						else
							mousedy = -1;
					}
					else
						mousedy = 1;
				else
					mousedy = 0x8000u;
			else
				mousedy = 0x7FFF;

			cmd->mousedy = mousedy;
		}

		if (delta_y != 0.0f) {
			float mouse_x = -((delta_y / m_yaw->GetFloat()) / sensitivity->GetFloat());
			short mousedx;
			if (mouse_x <= 32767.0f) {
				if (mouse_x >= -32768.0f) {
					if (mouse_x >= 1.0f || mouse_x < 0.0f) {
						if (mouse_x <= -1.0f || mouse_x > 0.0f)
							mousedx = static_cast<short>(mouse_x);
						else
							mousedx = -1;
					}
					else
						mousedx = 1;
				}
				else
					mousedx = 0x8000u;
			}
			else
				mousedx = 0x7FFF;

			cmd->mousedx = mousedx;
		}
	}; CPredictionSystem::Get().EndPrediction(g_LocalPlayer, cmd);

	return false;
}