#include "../hooks.hpp"

int __fastcall hooks::hkDoPostScreenEffects(void* _this, int edx, int a1)
{
	static auto oDoPostScreenEffects = clientmode_hook.get_original<decltype(&hkDoPostScreenEffects)>(index::DoPostScreenSpaceEffects);

	if (g_LocalPlayer && g_Options.enabled_glow)
		g_Glow->Run();

	return oDoPostScreenEffects(g_ClientMode, edx, a1);
}
