#include "../hooks.hpp"

void __fastcall hooks::hkOverrideView(void* _this, int edx, CViewSetup* vsView)
{
	static auto ofunc = clientmode_hook.get_original<decltype(&hkOverrideView)>(index::OverrideView);
	g_Visuals->no_visual_recoil(vsView);
	if (g_LocalPlayer && g_LocalPlayer->IsAlive() && g_EngineClient->IsInGame()) {
		g_Visuals->ThirdPerson();
	}

	ofunc(g_ClientMode, edx, vsView);
}