#include "../hooks.hpp"

long __stdcall hooks::hkReset(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* pPresentationParameters)
{
	static auto oReset = direct3d_hook.get_original<decltype(&hkReset)>(index::Reset);

	Menu::Get().OnDeviceLost();

	auto hr = oReset(device, pPresentationParameters);

	if (hr >= 0)
		Menu::Get().OnDeviceReset();

	return hr;
}
